/* global tinymce */
import serialize from 'form-serialize';
import Clipboard from 'clipboard';
import Tabs from './libs/tabs';
import Confirmation from './libs/confirmation';

/**
 * CSRF Token
 */
let csrfToken = null;
const csrfTokenMeta = document.querySelector('meta[name="csrf-token"]');

if (csrfTokenMeta) {
    csrfToken = csrfTokenMeta.getAttribute('content');
}

const projectTabs = document.querySelector('.project-content');

if (projectTabs) {
    const tabs = new Tabs(projectTabs, {
        heading: 'ul.tab-headings',
        panel: 'div.tab-body',
    });
}

const multiplePageForm = document.querySelector('#multiple-pages form');

if (multiplePageForm) {
    const errorMsg = multiplePageForm.querySelector('.alert-errors');

    multiplePageForm.closest('#multiple-pages').addEventListener('close', () => {
        if (!errorMsg.classList.contains('hidden')) {
            errorMsg.classList.add('hidden');
        }
    });

    multiplePageForm.addEventListener('submit', (e) => {
        e.preventDefault();

        if (!errorMsg.classList.contains('hidden')) {
            errorMsg.classList.add('hidden');
        }

        fetch(multiplePageForm.action, {
            method: 'post',
            credentials: 'same-origin',
            headers: {
                'Content-type': 'application/x-www-form-urlencoded; charset=UTF-8',
                Accept: 'application/json',
                'X-Requested-With': 'XMLHttpRequest',
                'X-CSRF-TOKEN': document.querySelector('meta[name="csrf-token"]').getAttribute('content'),
            },
            body: serialize(multiplePageForm),
        })
        .then(response => response.json())
        .then((result) => {
            if (result.status === undefined) {
                let message = '';

                Object.entries(result).forEach((value) => {
                    message += `${value[1][0]}`;
                });

                errorMsg.classList.remove('hidden');
                errorMsg.innerHTML = message;
            } else {
                window.location.reload(true);
            }
        })
        .catch((error) => {
            console.error(error);
        });
    });
}

const copyToClipboard = document.querySelectorAll('.copy-to-clipboard');

if (copyToClipboard && Clipboard.isSupported()) {
    const clipboard = new Clipboard('.copy-to-clipboard');

    clipboard.on('success', (e) => {
        const trigger = e.trigger;

        trigger.classList.remove('fa-link');
        trigger.classList.add('fa-check');

        setTimeout(() => {
            trigger.classList.remove('fa-check');
            trigger.classList.add('fa-link');
        }, 1250);
    });
}

/**
 * Page Delete Buttons
 */
const deleteButtons = document.querySelectorAll('.delete-btn');

if (deleteButtons) {
    Array.from(deleteButtons).forEach((target) => {
        const confirmation = new Confirmation(target, {
            message: 'Are you sure?<br><small>This will remove any nested pages</small>',
        });

        confirmation.on('confirm', () => {
            let row = target.closest('tr');
            const pageId = target.getAttribute('data-id');
            const project = target.getAttribute('data-project');
            const indent = parseInt(row.getAttribute('class').split('-').pop(), 10);

            // Fetch
            fetch(`/content/${project}/${pageId}`, {
                method: 'post',
                credentials: 'same-origin',
                headers: {
                    'Content-type': 'application/x-www-form-urlencoded; charset=UTF-8',
                    'X-CSRF-TOKEN': csrfToken,
                    Accept: 'application/json',
                    'X-Requested-With': 'XMLHttpRequest',
                },
                body: '_method=DELETE',
            })
            .then(response => response.json())
            .then((result) => {
                if (result.status !== undefined) {
                    confirmation.destroy();
                }
            })
            .catch(() => {});

            // Remove nested pages
            while (row) {
                const nextRow = row.nextSibling.nextSibling;

                // Remove current row
                row.remove();

                if (parseInt(nextRow.getAttribute('class').split('-').pop(), 10) > indent) {
                    row = nextRow;
                } else {
                    row = null;
                }
            }
        });

        target.addEventListener('click', (event) => {
            event.preventDefault();
        });
    });
}

/**
 * TinyMCE
 */
const editors = document.querySelectorAll('textarea.editor');

Array.from(editors).forEach((element) => {
    tinymce.init({
        selector: `textarea#${element.id}`,
        theme: 'modern',
        height: 260,
        max_height: 600,
        autoresize_min_height: 260,
        autoresize_max_height: 600,
        autoresize_bottom_margin: 0,
        plugins: [
            'advlist anchor autolink autoresize lists link image',
            'media paste textpattern imagetools',
        ],
        toolbar1: 'undo redo | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link',
        branding: false,
        menubar: false,
        statusbar: false,
        textpattern_patterns: [
            { start: '*', end: '*', format: 'italic' },
            { start: '**', end: '**', format: 'bold' },
            { start: '#', format: 'h1' },
            { start: '##', format: 'h2' },
            { start: '###', format: 'h3' },
            { start: '####', format: 'h4' },
            { start: '#####', format: 'h5' },
            { start: '######', format: 'h6' },
            { start: '1. ', cmd: 'InsertOrderedList' },
            { start: '* ', cmd: 'InsertUnorderedList' },
            { start: '- ', cmd: 'InsertUnorderedList' },
        ],
    });
});
