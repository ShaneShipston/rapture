/* global plupload */
export default class FileUpload {
    constructor(options = {}) {
        this.options = Object.assign({
            container: 'pickfiles',
            upload: null,
            target: null,
            multi: true,
            instructions: '.instructions',
            fieldKey: 'plupload',
            class: 'asset',
            typePrefix: 'type',
            maxFiles: 999,
            filters: {},
        }, options);

        this.extensions = [
            {
                name: 'image',
                exts: /(.jpg|.jpeg|.gif|.png)$/i,
            },
            {
                name: 'archive',
                exts: /(.zip|.rar)$/i,
            },
            {
                name: 'audio',
                exts: /(.mp3|.wav)$/i,
            },
            {
                name: 'excel',
                exts: /(.xls|.xlsx|.csv)$/i,
            },
            {
                name: 'pdf',
                exts: /(.pdf)$/i,
            },
            {
                name: 'video',
                exts: /(.mp4|.webm|.mov|.avi|.mkv|.ogv|.ogg|.wmv)$/i,
            },
            {
                name: 'text',
                exts: /(.txt)$/i,
            },
            {
                name: 'powerpoint',
                exts: /(.ppt|.pptx)$/i,
            },
            {
                name: 'word',
                exts: /(.doc|.docx)$/i,
            },
        ];

        this.createUploadInstance();
        this.uploader.init();
    }
    createUploadInstance() {
        this.uploader = new plupload.Uploader({
            runtimes: 'html5,html4',
            browse_button: this.options.container,
            url: this.options.upload,
            drop_element: this.options.container,
            multi_selection: this.options.multi,
            file_data_name: this.options.fieldKey,
            filters: this.options.filters,
            headers: {
                'X-CSRF-TOKEN': document.querySelector('meta[name="csrf-token"]').getAttribute('content'),
            },
            init: {
                FilesAdded: this.filesAdded.bind(this),
                UploadProgress: this.uploadProgress.bind(this),
                FileUploaded: this.fileUploaded.bind(this),
            },
        });
    }
    filesAdded(up, files) {
        const instructions = this.options.container.querySelector(this.options.instructions);
        const newFiles = this.options.maxFiles - (up.files.length - files.length);
        let filesToUpload = files;

        if (instructions) {
            instructions.classList.add('hidden');
        }

        if (up.files.length > this.options.maxFiles) {
            up.splice(this.options.maxFiles);
        }

        if (files.length > newFiles) {
            filesToUpload = filesToUpload.slice(0, newFiles);
        }

        if (newFiles > 0) {
            plupload.each(filesToUpload, (file) => {
                this.appendPreview(file);
            });
        }

        up.start();
    }
    fileUploaded(up, file, result) {
        const uploadedEvent = new CustomEvent('uploaded', {
            detail: {
                path: result.response,
                ref: file.id,
                name: file.name,
            },
            bubbles: true,
        });

        const uploadTarget = document.getElementById(file.id);

        uploadTarget.querySelector('.progress').remove();
        uploadTarget.classList.remove('uploading');
        uploadTarget.classList.add('complete');

        if (this.fileExt(file) === 'image') {
            uploadTarget.querySelector('.preview').style.backgroundImage = `url(${result.response})`;
        }

        this.options.container.dispatchEvent(uploadedEvent);
    }
    uploadProgress(up, file) {
        this.options.container.querySelector(`#${file.id} .bar`).style.width = `${file.percent}%`;
    }
    setBrowseButton(element) {
        this.uploader.setOption('browse_button', element);
        this.uploader.refresh();
    }
    appendPreview(file) {
        const parser = new DOMParser();
        const fileType = this.fileExt(file);
        const uploadPreview = `<div class="${this.options.class} ${this.options.typePrefix}-${fileType} uploading" id="${file.id}">
            <div class="preview"></div>
            <div class="progress">
                <div class="bar" style="width: 0;"></div>
            </div>
        </div>`;
        const newDoc = parser.parseFromString(uploadPreview, 'text/html');
        const placeholder = newDoc.querySelector(`.${this.options.class}`);

        if (this.options.multi) {
            this.options.container.appendChild(placeholder);
        } else {
            this.options.container.innerHTML = placeholder;
        }

        if (fileType === 'image') {
            const reader = new FileReader();

            reader.onload = (e) => {
                document.getElementById(file.id).querySelector('.preview').style.backgroundImage = `url(${e.target.result})`;
            };

            reader.readAsDataURL(file.getNative());
        }

        this.uploader.refresh();
    }
    fileExt(file) {
        let fileType = 'text';

        this.extensions.forEach((group) => {
            if (group.exts.test(file.name)) {
                fileType = group.name;
            }
        });

        return fileType;
    }
    on(event, callback) {
        this.options.container.addEventListener(event, callback);
    }
}
