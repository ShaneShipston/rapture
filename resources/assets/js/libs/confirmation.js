import Tooltip from './tooltip';

export default class Confirmation {
    constructor(trigger, options = {}) {
        this.options = Object.assign({
            align: 'top',
            confirm: 'Yes',
            cancel: 'Cancel',
            message: 'Are you sure?',
        }, options);

        this.trigger = trigger;
        this.uniqueID = `confirm-${Math.random().toString(36).substr(2, 16)}`;

        this.createElement();
        this.setTooltip();

        this.tooltip = new Tooltip(trigger, {
            align: this.options.align,
            trigger: 'click',
        });

        this.confirmClick = this.onConfirm.bind(this);
        this.cancelClick = this.onCancel.bind(this);

        this.attachListeners();
    }
    createElement() {
        this.target = document.createElement('div');
        this.target.classList.add('confirmation');
        this.target.id = this.uniqueID;

        // Message
        this.message = document.createElement('p');
        this.message.innerHTML = this.options.message;

        // Confirm Button
        this.confirm = document.createElement('button');
        this.confirm.classList.add('btn', 'small', 'danger');
        this.confirm.innerHTML = this.options.confirm;

        // Cancel Button
        this.cancel = document.createElement('button');
        this.cancel.classList.add('btn', 'small', 'muted');
        this.cancel.innerHTML = this.options.cancel;

        // Add elements
        this.target.appendChild(this.message);
        this.target.appendChild(this.confirm);
        this.target.appendChild(this.cancel);

        // Add target to bottom of document
        document.body.appendChild(this.target);
    }
    setTooltip() {
        this.trigger.setAttribute('data-tooltip', `#${this.uniqueID}`);
    }
    removeElement() {
        this.target.remove();
    }
    attachListeners() {
        this.confirm.addEventListener('click', this.confirmClick);
        this.cancel.addEventListener('click', this.cancelClick);
    }
    removeListeners() {
        this.confirm.removeEventListener('click', this.confirmClick);
        this.cancel.removeEventListener('click', this.cancelClick);
    }
    destroy() {
        this.removeListeners();
        this.removeElement();
    }
    onConfirm() {
        this.tooltip.hide();
        this.target.dispatchEvent(new Event('confirm'));
    }
    onCancel() {
        this.tooltip.hide();
        this.target.dispatchEvent(new Event('cancel'));
    }
    on(event, callback) {
        this.target.addEventListener(event, callback);
    }
}
