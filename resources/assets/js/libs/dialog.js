export default class Dialog {
    constructor(element, options = {}) {
        this.options = Object.assign({
            body: 'dialog-body',
            close: 'dialog-close',
        }, options);

        this.events = {
            open: new Event('open'),
            close: new Event('close'),
        };

        this.element = element;

        const target = element.getAttribute('data-dialog');

        if (target.substr(0, 1) === '#') {
            // ID
            this.target = document.getElementById(target.substr(1));
        } else if (target.substr(0, 1) === '.') {
            // Class
            this.target = document.querySelector(target);
        }

        this.hide();

        window.addEventListener('keydown', (event = window.event) => {
            if (event.keyCode === 27) {
                this.hide();
            }
        });

        this.target.querySelector(`.${this.options.close}`).addEventListener('click', () => {
            this.close();
        });

        this.target.querySelector(`.${this.options.body}`).addEventListener('click', (e) => {
            e.stopPropagation();
        });

        this.target.addEventListener('click', (e) => {
            e.stopPropagation();
            this.close();
        });

        this.element.addEventListener('click', (e) => {
            e.preventDefault();
            this.open();
        });
    }
    show() {
        this.target.style.display = 'block';
    }
    hide() {
        this.target.style.display = 'none';
    }
    open() {
        this.show();
        this.target.dispatchEvent(this.events.open);
    }
    close() {
        this.hide();
        this.target.dispatchEvent(this.events.close);
    }
}
