export default class AssetLibrary {
    constructor(options = {}) {
        this.options = Object.assign({
            container: null,
            element: null,
            multi: true,
            buttons: [],
        }, options);

        const buttons = {
            select: {
                label: 'Select All',
                visibility: true,
                callback: this.btnSelect,
                class: 'action btn muted',
                bubbles: false,
            },
            clear: {
                label: 'Clear',
                visibility: 'auto',
                callback: this.btnClear,
                class: 'action btn muted',
                bubbles: false,
            },
            remove: {
                label: 'Remove',
                visibility: 'auto',
                callback: this.btnRemove,
                class: 'action btn muted',
                bubbles: false,
            },
            download: {
                label: 'Download',
                visibility: 'auto',
                callback: this.btnDownload,
                class: 'action btn muted',
                bubbles: false,
            },
        };

        if ('btnSelect' in options) {
            this.options.btnSelect = Object.assign(buttons.select, options.btnSelect);
        } else {
            this.options.btnSelect = buttons.select;
        }

        if ('btnClear' in options) {
            this.options.btnClear = Object.assign(buttons.clear, options.btnClear);
        } else {
            this.options.btnClear = buttons.clear;
        }

        if ('btnRemove' in options) {
            this.options.btnRemove = Object.assign(buttons.remove, options.btnRemove);
        } else {
            this.options.btnRemove = buttons.remove;
        }

        if ('btnDownload' in options) {
            this.options.btnDownload = Object.assign(buttons.download, options.btnDownload);
        } else {
            this.options.btnDownload = buttons.download;
        }

        this.lastSelection = null;

        this.injectToolbar();
        this.buttonStrip();
        this.refresh();
    }
    attachListener(element) {
        element.onClickEvent = this.onClick.bind(this, element);
        element.addEventListener('click', element.onClickEvent);
    }
    onClick(element, event) {
        event.stopPropagation();

        let previousIndex = this.getTargets().findIndex(item => item === this.lastSelection);
        let nextIndex = this.getTargets().findIndex(item => item === element);

        // Swap index values
        if (previousIndex > nextIndex) {
            [previousIndex, nextIndex] = [nextIndex, previousIndex];
        }

        // Clear previously selected
        if (!this.options.multi && this.lastSelection !== null) {
            this.lastSelection.classList.remove('selected');
            this.lastSelection = null;
        }

        // Watch for shift key
        if (this.options.multi && event.shiftKey) {
            this.getTargets().slice(previousIndex, nextIndex + 1).forEach((item) => {
                item.classList.add('selected');
            });
        } else if (element.classList.contains('selected')) {
            element.classList.remove('selected');
            this.options.container.dispatchEvent(new Event('deselect'));
        } else {
            element.classList.add('selected');
            this.lastSelection = element;
            this.options.container.dispatchEvent(new Event('select'));
        }
    }
    getTargets() {
        return Array.from(this.options.container.querySelectorAll(this.options.element));
    }
    getSelected() {
        return Array.from(this.options.container.querySelectorAll(`${this.options.element}.selected`));
    }
    destroy() {
        // Clean up assets
        this.getTargets().forEach((target) => {
            target.removeEventListener('click', target.onClickEvent);
            target.classList.remove('init', 'selected', 'error');
        });

        // Remove toolbar
        this.toolbar.remove();

        // Remove setup
        this.options.container.classList.remove('has-toolbar');
    }
    refresh() {
        this.getTargets().forEach((target) => {
            if (!target.classList.contains('init')) {
                this.attachListener(target);

                target.classList.add('init');
            }
        });
    }
    selectAll() {
        this.getTargets().forEach((target) => {
            target.classList.add('selected');
        });

        this.options.container.dispatchEvent(new Event('selectall'));
    }
    clear() {
        this.getTargets().forEach((target) => {
            target.classList.remove('selected');
        });

        this.options.container.dispatchEvent(new Event('clear'));
    }
    injectToolbar() {
        this.toolbar = document.createElement('div');
        this.toolbar.classList.add('toolbar');

        this.options.container.appendChild(this.toolbar);
        this.options.container.classList.add('has-toolbar');
    }
    buttonStrip() {
        if (this.options.buttons.length > 0) {
            this.options.buttons.forEach((button) => {
                this.addBtn(button);
            });
        }

        if (this.options.btnSelect !== false && this.options.multi) {
            this.addBtn(this.options.btnSelect);
        }

        if (this.options.btnClear !== false && this.options.multi) {
            this.addBtn(this.options.btnClear);
        }

        if (this.options.btnRemove !== false) {
            this.addBtn(this.options.btnRemove);
        }

        if (this.options.btnDownload !== false) {
            this.addBtn(this.options.btnDownload);
        }
    }
    addBtn(options) {
        const newBtn = document.createElement('button');
        newBtn.type = 'button';
        newBtn.innerHTML = options.label;

        if ('class' in options) {
            newBtn.setAttribute('class', options.class);
        }

        if ('callback' in options) {
            newBtn.addEventListener('click', options.callback.bind(this, newBtn, options));
        }

        if ('bubbles' in options && options.bubbles === false) {
            newBtn.addEventListener('click', (event) => {
                event.stopPropagation();
            });
        }

        if ('visibility' in options && options.visibility === 'auto') {
            newBtn.classList.add('hidden');

            ['select', 'deselect', 'selectall', 'clear', 'remove'].forEach((event) => {
                this.on(event, () => {
                    this.btnVisibility(newBtn);
                });
            });
        }

        this.toolbar.appendChild(newBtn);
    }
    btnVisibility(button) {
        if (this.getSelected().length > 0) {
            button.classList.remove('hidden');
        } else {
            button.classList.add('hidden');
        }
    }
    btnSelect() {
        this.selectAll();
    }
    btnClear() {
        this.clear();
    }
    btnRemove(button, options) {
        const selected = this.getSelected();

        // Add removing class to selected
        selected.forEach((item) => {
            item.classList.add('removing');
        });

        // Check for onRemove callback
        const progress = new Promise((resolve, reject) => {
            if ('onRemove' in options) {
                options.onRemove(selected, resolve, reject);
            } else {
                resolve();
            }
        });

        // Remove Selected
        progress.then(() => {
            selected.forEach((item) => {
                item.remove();
            });

            this.options.container.dispatchEvent(new Event('remove'));
        });

        // Display error
        progress.catch(() => {
            selected.forEach((item) => {
                item.classList.remove('removing');
                item.classList.add('error');
            });
        });
    }
    btnDownload(button, options) {
        const selected = this.getSelected();

        // Check for onRemove callback
        const progress = new Promise((resolve, reject) => {
            if ('onDownload' in options) {
                options.onDownload(selected, resolve, reject);
            } else {
                resolve();
            }
        });

        // Clear selection
        progress.then(() => {
            this.clear();
            this.options.container.dispatchEvent(new Event('download'));
        });

        // Display error
        progress.catch(() => {
            selected.forEach((item) => {
                item.classList.add('error');
            });
        });
    }
    on(event, callback) {
        this.options.container.addEventListener(event, callback);
    }
}
