export default class Tooltip {
    constructor(element, options = {}) {
        this.options = Object.assign({
            align: 'top',
            class: 'tooltip',
            trigger: 'hover',
            offset: 12,
        }, options);

        this.active = true;
        this.positioned = false;
        this.element = element;

        this.createOrFetchTarget();
        this.applyClass(this.options.class);
        this.applyClass(`align-${this.options.align}`);
        this.hide();

        if (this.options.trigger === 'click') {
            this.interactionClick();
        } else if (this.options.trigger === 'hover') {
            this.interactionMouse();
        }
    }
    createOrFetchTarget() {
        const trigger = this.element.getAttribute('data-tooltip');

        if (trigger.substr(0, 1) === '#') {
            this.target = document.getElementById(trigger.substr(1));
        } else if (trigger.substr(0, 1) === '.') {
            this.target = document.querySelector(trigger);
        } else {
            this.addElement();
            this.setContent(trigger);
        }
    }
    getTrigger() {
        return this.element;
    }
    getTarget() {
        return this.target;
    }
    setContent(content) {
        this.target.innerHTML = content;
    }
    isVisible() {
        return this.target.offsetHeight !== 0;
    }
    show() {
        if (!this.active) {
            return;
        }

        this.target.style.display = 'block';
        this.positionElement();
        this.target.dispatchEvent(new Event('open'));
    }
    hide() {
        this.target.style.display = 'none';
        this.target.dispatchEvent(new Event('close'));
    }
    toggle() {
        if (this.isVisible()) {
            this.hide();
        } else {
            this.show();
        }
    }
    activate() {
        this.active = true;
    }
    deactivate() {
        this.active = false;
    }
    positionElement() {
        const triggerElement = this.element.getBoundingClientRect();
        const targetElement = this.target.getBoundingClientRect();
        const left = triggerElement.left + ((triggerElement.width / 2) - (targetElement.width / 2));
        const top = triggerElement.top - targetElement.height - this.options.offset;

        this.target.style.left = `${parseInt(left, 10)}px`;
        this.target.style.top = `${parseInt(top, 10)}px`;
    }
    addElement() {
        const newElement = document.createElement('div');
        document.body.appendChild(newElement);

        this.target = newElement;
    }
    applyClass(newClass) {
        if (!this.target.classList.contains(newClass)) {
            this.target.classList.add(newClass);
        }
    }
    interactionClick() {
        this.element.addEventListener('click', () => {
            this.toggle();
        });
    }
    interactionMouse() {
        this.element.addEventListener('mouseover', () => {
            this.show();
        });

        this.element.addEventListener('mouseout', () => {
            this.hide();
        });
    }
    on(event, callback) {
        this.target.addEventListener(event, callback);
    }
}
