export function cleanContent(content, trim = true) {
    let cleaned = content;

    cleaned = cleaned.replace(/(<([^>]+)>)/ig, '');
    cleaned = cleaned.replace(/&nbsp;/g, ' ');
    cleaned = cleaned.replace(/(?:\r\n|\r|\n)/g, ' ');

    if (trim) {
        cleaned = cleaned.trim();
    }

    return cleaned;
}

export function characterCount(content) {
    return cleanContent(content).length;
}

export function wordCount(content) {
    return cleanContent(content, false).split(' ').length;
}

export function slide(element, speed = 300, direction = 'up', easing = 'linear') {
    // prevent user from sliding down if already sliding
    if (direction === 'down' &&
        (
            [...element.classList].some(e => new RegExp(/setHeight/).test(e))
            || !element.classList.contains('slider-hidden')
        )
    ) return false;

    // prevent user from sliding up if already sliding
    if (direction === 'up' &&
        (
            element.classList.contains('slider-hidden')
            || [...element.classList].some(e => new RegExp(/setHeight/).test(e))
        )
    ) return false;

    const s = element.style;
    let contentHeight = element.scrollHeight;

    // subtract padding from contentHeight
    if (direction === 'up') {
        const style = window.getComputedStyle(element);
        const paddingTop = +style.getPropertyValue('padding-top').split('px')[0];
        const paddingBottom = +style.getPropertyValue('padding-bottom').split('px')[0];
        contentHeight = element.scrollHeight - paddingTop - paddingBottom;
    }

    // create a setHeight CSS class
    const sheet = document.createElement('style');
    // create an id for each class to allow multiple elements to slide
    // at the same time, such as when activated by a forEach loop
    const setHeightId = (Date.now() * Math.random()).toFixed(0);
    sheet.innerHTML = `.setHeight-${setHeightId} {height: ${contentHeight}px;}`;
    document.head.appendChild(sheet);

    // add the CSS classes that will give the computer a fixed starting point
    if (direction === 'up') {
        element.classList.add(`setHeight-${setHeightId}`);
    } else {
        element.classList.add('slider-hidden', `setHeight-${setHeightId}`);
    }

    s.transition = `all ${speed}ms ${easing}`;
    s.overflow = 'hidden';

    // add/remove the CSS class(s) that will animate the element
    if (direction === 'up') {
        // Don't know why, but waiting 10 milliseconds before adding
        // the 'hidden' class when sliding up prevents height-jumping
        setTimeout(() => {
            element.classList.add('slider-hidden');
        }, 10);
    } else {
        element.classList.remove('slider-hidden');
    }

    const done = new Promise((resolve) => {
        setTimeout(() => {
            // remove the temporary inline styles and remove the temp stylesheet
            element.removeAttribute('style');
            sheet.parentNode.removeChild(sheet);
            element.classList.remove(`setHeight-${setHeightId}`);
            resolve(element);
        }, speed);
    });

    return done;
}

export function slideUp(element, speed = 300, easing = 'linear') {
    slide(element, speed, 'up', easing);
}

export function slideDown(element, speed = 300, easing = 'linear') {
    slide(element, speed, 'down', easing);
}
