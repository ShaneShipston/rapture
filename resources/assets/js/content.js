/* global tinymce, currentPage */
import FileUpload from './libs/fileupload';
import Confirmation from './libs/confirmation';
import AssetLibrary from './libs/assetlibrary';
import { characterCount, wordCount, cleanContent } from './libs/helpers';

/**
 * CSRF Token
 */
let csrfToken = null;
const csrfTokenMeta = document.querySelector('meta[name="csrf-token"]');

if (csrfTokenMeta) {
    csrfToken = csrfTokenMeta.getAttribute('content');
}

/**
 * TinyMCE
 */
const editors = document.querySelectorAll('textarea.editor');

Array.from(editors).forEach((element) => {
    const feedback = element.closest('.form-field').querySelector('.feedback .num');
    const limitType = element.getAttribute('data-type');
    const limitCount = element.getAttribute('data-limit');

    tinymce.init({
        selector: `textarea#${element.id}`,
        theme: 'modern',
        height: 260,
        max_height: 600,
        autoresize_min_height: 260,
        autoresize_max_height: 600,
        autoresize_bottom_margin: 0,
        plugins: [
            'advlist anchor autolink autoresize lists link image',
            'media paste textpattern imagetools',
        ],
        toolbar1: 'undo redo | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link',
        branding: false,
        menubar: false,
        statusbar: false,
        textpattern_patterns: [
            { start: '*', end: '*', format: 'italic' },
            { start: '**', end: '**', format: 'bold' },
            { start: '#', format: 'h1' },
            { start: '##', format: 'h2' },
            { start: '###', format: 'h3' },
            { start: '####', format: 'h4' },
            { start: '#####', format: 'h5' },
            { start: '######', format: 'h6' },
            { start: '1. ', cmd: 'InsertOrderedList' },
            { start: '* ', cmd: 'InsertUnorderedList' },
            { start: '- ', cmd: 'InsertUnorderedList' },
        ],
        init_instance_callback: (editor) => {
            // Bail early when no restrictions set
            if (!element.hasAttribute('data-type')) {
                return;
            }

            editor.on('KeyDown', (e) => {
                let currentLength = null;
                let reject = false;

                if (limitType === 'words') {
                    currentLength = wordCount(editor.getContent());
                    reject = (
                        currentLength === limitCount &&
                        (e.keyCode !== 8 || e.keyCode === 13 || e.keyCode === 32)
                    ) || (
                        currentLength > limitCount && e.keyCode !== 8
                    );
                } else {
                    currentLength = characterCount(editor.getContent());
                    reject = currentLength >= limitCount && (e.keyCode !== 8 || e.keyCode === 13);
                }

                if (reject) {
                    e.preventDefault();
                    e.stopPropagation();

                    return false;
                }

                return true;
            });

            editor.on('KeyUp SetContent', () => {
                let currentLength = null;

                if (limitType === 'words') {
                    currentLength = cleanContent(editor.getContent(), true).split(' ').length;
                } else {
                    currentLength = characterCount(editor.getContent());
                }

                feedback.innerText = currentLength;
            });

            editor.on('PastePreProcess', (e) => {
                let currentLength = null;
                let newContentLength = null;

                if (limitType === 'words') {
                    currentLength = wordCount(editor.getContent());
                    newContentLength = wordCount(e.content);
                } else {
                    currentLength = characterCount(editor.getContent());
                    newContentLength = characterCount(e.content);
                }

                const remaining = limitCount - currentLength;

                if (remaining <= 0) {
                    e.content = '';
                } else if (newContentLength >= remaining) {
                    if (limitType === 'words') {
                        e.content = cleanContent(e.content).split(' ').slice(0, remaining).join(' ');
                    } else {
                        e.content = cleanContent(e.content).substr(0, remaining);
                    }
                }
            });
        },
    });
});

/**
 * Input Restrictions
 */
const restricted = document.querySelectorAll('input[data-limit]');

Array.from(restricted).forEach((element) => {
    const feedback = element.closest('.form-field').querySelector('.feedback .num');
    const limitType = element.getAttribute('data-type');
    const limitCount = element.getAttribute('data-limit');

    element.addEventListener('keydown', (e) => {
        let currentLength = null;
        let reject = false;

        if (limitType === 'words') {
            currentLength = wordCount(element.value);
            reject = (
                currentLength === limitCount &&
                (e.keyCode !== 8 || e.keyCode === 13 || e.keyCode === 32)
            ) || (
                currentLength > limitCount && e.keyCode !== 8
            );
        } else {
            currentLength = characterCount(element.value);
            reject = currentLength >= limitCount && (e.keyCode !== 8 || e.keyCode === 13);
        }

        if (reject) {
            e.preventDefault();
            e.stopPropagation();

            return false;
        }

        return true;
    });

    ['change', 'keyup'].forEach((event) => {
        element.addEventListener(event, () => {
            let currentLength = null;

            if (limitType === 'words') {
                currentLength = wordCount(element.value);
            } else {
                currentLength = characterCount(element.value);
            }

            if (currentLength > limitCount) {
                if (limitType === 'words') {
                    element.value = cleanContent(element.value).split(' ').slice(0, limitCount).join(' ');
                } else {
                    element.value = cleanContent(element.value).substr(0, limitCount);
                }
            }

            if (limitType === 'words') {
                currentLength = cleanContent(element.value, true).split(' ').length;
            } else {
                currentLength = characterCount(element.value);
            }

            feedback.innerText = currentLength;
        });
    });
});

/**
 * File Uploads
 */
const uploads = document.querySelectorAll('.upload-box');

Array.from(uploads).forEach((target) => {
    const fileLimit = target.hasAttribute('data-limit') ? parseInt(target.getAttribute('data-limit'), 10) : 999;
    let extLimit = {};

    if (target.hasAttribute('data-extensions')) {
        extLimit = {
            mime_types: [
                {
                    title: 'Files',
                    extensions: target.getAttribute('data-extensions'),
                },
            ],
        };
    }

    const upload = new FileUpload({
        container: target,
        instructions: '.how-to',
        upload: `/projects/${currentPage.project}/content/upload`,
        maxFiles: fileLimit,
        filters: extLimit,
        class: 'asset',
    });

    const assets = new AssetLibrary({
        container: target,
        element: '.asset',
        buttons: [{
            label: 'Upload',
            visibility: true,
            class: 'action btn muted',
            bubbles: true,
        }],
        btnDownload: {
            onDownload: (selected, resolve, reject) => {
                const files = [];

                // Grab inputs
                selected.forEach((asset) => {
                    const input = asset.querySelector('input');

                    if (input) {
                        files.push(input.value);
                    }
                });

                // Send file paths to server
                fetch(`/projects/${currentPage.project}/content/download`, {
                    method: 'post',
                    credentials: 'same-origin',
                    headers: {
                        'Content-type': 'application/x-www-form-urlencoded; charset=UTF-8',
                        'X-CSRF-TOKEN': csrfToken,
                        Accept: 'application/json',
                    },
                    body: `assets=${JSON.stringify(files)}`,
                })
                .then(response => response.json())
                .then((result) => {
                    if (result.status !== undefined) {
                        // Inject Form
                        const newLink = document.createElement('form');
                        newLink.setAttribute('action', `/${result.file}`);
                        newLink.setAttribute('method', 'get');

                        document.body.appendChild(newLink);

                        // Submit it
                        newLink.submit();

                        // Resolve promise
                        resolve();
                    } else {
                        reject();
                    }
                })
                .catch(() => {
                    reject();
                });
            },
        },
    });

    upload.on('uploaded', (e) => {
        const input = document.createElement('input');
        input.type = 'hidden';
        input.name = target.getAttribute('data-field-name');
        input.value = e.detail.path;

        document.getElementById(e.detail.ref).appendChild(input);

        assets.refresh();
    });
});

/**
 * Page Delete Buttons
 */
const deleteButtons = document.querySelectorAll('.delete-btn');

if (deleteButtons) {
    Array.from(deleteButtons).forEach((target) => {
        const confirmation = new Confirmation(target, {
            message: 'Are you sure?<br><small>This will remove any nested pages</small>',
        });

        confirmation.on('confirm', () => {
            let row = target.closest('tr');
            const pageId = target.getAttribute('data-id');
            const project = target.getAttribute('data-project');
            const indent = parseInt(row.getAttribute('class').split('-').pop(), 10);

            // Fetch
            fetch(`/projects/${project}/content/${pageId}`, {
                method: 'post',
                credentials: 'same-origin',
                headers: {
                    'Content-type': 'application/x-www-form-urlencoded; charset=UTF-8',
                    'X-CSRF-TOKEN': csrfToken,
                    Accept: 'application/json',
                    'X-Requested-With': 'XMLHttpRequest',
                },
                body: '_method=DELETE',
            })
            .then(response => response.json())
            .then((result) => {
                if (result.status !== undefined) {
                    confirmation.destroy();
                }
            })
            .catch(() => {});

            // Remove nested pages
            while (row) {
                const nextRow = row.nextSibling.nextSibling;

                // Remove current row
                row.remove();

                if (parseInt(nextRow.getAttribute('class').split('-').pop(), 10) > indent) {
                    row = nextRow;
                } else {
                    row = null;
                }
            }
        });

        target.addEventListener('click', (event) => {
            event.preventDefault();
        });
    });
}
