import Datepicker from 'pikaday';
import Tooltip from './libs/tooltip';
import Dialog from './libs/dialog';
import { slideUp, slideDown } from './libs/helpers';

const tooltips = document.querySelectorAll('[data-tooltip]');

if (tooltips) {
    Array.from(tooltips).forEach((target) => {
        const tooltip = new Tooltip(target);
    });
}

const dialogs = document.querySelectorAll('[data-dialog]');

if (dialogs) {
    Array.from(dialogs).forEach((target) => {
        const dialog = new Dialog(target);
    });
}

const searchBox = document.getElementById('search-form');
const searchField = searchBox.querySelector('input');

searchField.addEventListener('keydown', (event = window.event) => {
    if (searchField.value.length === 0 && event.keyCode === 8) {
        const context = searchBox.querySelector('.context');
        context.classList.add('hidden');
    }
});

const datepickers = document.querySelectorAll('.datepicker');

Array.from(datepickers).forEach((target) => {
    const picker = new Datepicker({
        field: target,
        minDate: new Date(),
    });
});

const dropdown = document.querySelectorAll('.is-dropdown > a');

Array.from(dropdown).forEach((target) => {
    target.addEventListener('click', (event) => {
        event.preventDefault();

        const menu = target.closest('.menu-item').querySelector('.sub-menu');

        if (menu.classList.contains('slider-hidden')) {
            slideDown(menu, 200, 'ease-in-out');
            target.classList.add('opened');
        } else {
            slideUp(menu, 200, 'ease-in-out');
            target.classList.remove('opened');
        }
    });
});
