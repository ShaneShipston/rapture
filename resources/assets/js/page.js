import dragula from 'dragula';
import { slide } from './libs/helpers';

function cloneFieldOption(el) {
    if (!el.classList.contains('cloned')) {
        el.classList.remove('closed');
        el.classList.add('cloned');
        slide(el.querySelector('.field-body'), 300, 'down');

        const fieldCount = document.querySelector('input[name=field_count]');
        const fields = el.querySelectorAll('[name]');
        const increment = parseInt(fieldCount.value, 10);

        // increment field count
        fieldCount.value = increment + 1;

        // Replace index with new field count
        Array.from(fields).forEach((field) => {
            field.name = field.name.replace('[index]', `[${increment}]`);
        });
    }
}

function updateFieldOrder(container) {
    const fields = container.querySelectorAll('.field-option');

    Array.from(fields).forEach((field, index) => {
        const order = field.querySelector('.field-order');
        order.value = index;
    });
}

const newContent = document.getElementById('new-content');
const contentOptions = document.getElementById('content-options');

if (newContent && contentOptions) {
    dragula([contentOptions, newContent], {
        copy: (el, source) => source === contentOptions,
        accepts: (el, target) => target !== contentOptions,
        moves: (el, container, handle) => handle.classList.contains('field-title') || handle.classList.contains('field-title-display'),
    })
    .on('drop', (el) => {
        const instructions = newContent.querySelector('.instructions');

        if (instructions) {
            const totalItems = newContent.querySelectorAll('.field-option');

            if (totalItems.length > 0) {
                instructions.remove();
            }
        }

        cloneFieldOption(el);
        updateFieldOrder(newContent);
    });

    const fieldTitles = contentOptions.querySelectorAll('.field-title');
    Array.from(fieldTitles).forEach((target) => {
        target.addEventListener('click', (e) => {
            e.preventDefault();

            const option = target.closest('.field-option').cloneNode(true);
            const instructions = newContent.querySelector('.instructions');
            newContent.appendChild(option);

            if (instructions) {
                const totalItems = newContent.querySelectorAll('.field-option');

                if (totalItems.length > 0) {
                    instructions.remove();
                }
            }

            cloneFieldOption(option);
            updateFieldOrder(newContent);
        });
    });
}

window.addEventListener('keyup', (event = window.event) => {
    if (event.target.classList.contains('field-label')) {
        const title = event.target.closest('.field-option').querySelector('.field-title-display');

        if (event.target.value.length > 0) {
            title.innerText = ` - ${event.target.value}`;
        } else {
            title.innerText = '';
        }
    }
});

window.addEventListener('click', (event = window.event) => {
    if ((event.target.classList.contains('field-title') || event.target.parentNode.classList.contains('field-title')) && event.target.closest('.drop-target')) {
        event.preventDefault();

        const fieldContainer = event.target.closest('.field-option').querySelector('.field-body');
        const fieldOption = event.target.closest('.field-option');

        if (fieldContainer.classList.contains('slider-hidden')) {
            fieldOption.classList.remove('closed');
            slide(fieldContainer, 300, 'down');
        } else {
            fieldOption.classList.add('closed');
            slide(fieldContainer, 300, 'up');
        }
    }

    if (event.target.classList.contains('remove-field')) {
        event.preventDefault();
        event.stopPropagation();
        event.target.closest('.field-option').remove();
    }
});
