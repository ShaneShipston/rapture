import Confirmation from './libs/confirmation';

/**
 * CSRF Token
 */
let csrfToken = null;
const csrfTokenMeta = document.querySelector('meta[name="csrf-token"]');

if (csrfTokenMeta) {
    csrfToken = csrfTokenMeta.getAttribute('content');
}

/**
 * Delete Buttons
 */
const deleteButtons = document.querySelectorAll('.delete-btn');

if (deleteButtons) {
    Array.from(deleteButtons).forEach((target) => {
        const confirmation = new Confirmation(target, {
            message: 'Are you sure?<br><small>This will archive any nested projects</small>',
        });

        confirmation.on('confirm', () => {
            const row = target.closest('tr');
            const userId = target.getAttribute('data-id');

            // Fetch
            fetch(`/users/${userId}`, {
                method: 'post',
                credentials: 'same-origin',
                headers: {
                    'Content-type': 'application/x-www-form-urlencoded; charset=UTF-8',
                    'X-CSRF-TOKEN': csrfToken,
                    Accept: 'application/json',
                    'X-Requested-With': 'XMLHttpRequest',
                },
                body: '_method=DELETE',
            })
            .then(response => response.json())
            .then((result) => {
                if (result.status !== undefined) {
                    row.remove();
                    confirmation.destroy();
                }
            })
            .catch(() => {});
        });

        target.addEventListener('click', (event) => {
            event.preventDefault();
        });
    });
}
