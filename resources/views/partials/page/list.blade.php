@foreach ($pages as $page)
    <tr class="indent-level-{{ $page->indent }}">
        <td class="mass-action">
            <div class="form-checkbox standalone">
                <input type="checkbox" name="page[]" value="{{ $page->id }}" id="page-action-{{ $page->id }}">
                <label for="page-action-{{ $page->id }}"></label>
            </div>
        </td>
        <td class="status">
            <span class="status-icon {{ $page->status }}" data-tooltip="{{ $page->statusLabel() }}"></span>
        </td>
        <td class="label">
            <a href="{{ $project->permalink('content/' . $page->id) }}">{{ $page->name }}</a>
        </td>
        <td class="due">{{ $page->formattedDeadline('M j') }}</td>
        <td class="assigned">--</td>
        <td class="updated">{{ $page->modified() }}</td>
        <td class="action">
            <a href="{{ $project->permalink('content/' . $page->id) }}"><em class="fa fa-eye"></em></a>
            <a href="{{ $project->permalink('content/' . $page->id . '/edit') }}"><em class="fa fa-pencil"></em></a>
            <a href="{{ url('') }}" class="delete-btn" data-id="{{ $page->id }}" data-project="{{ $project->permalink }}"><em class="fa fa-trash"></em></a>
        </td>
    </tr>
    @if ($page->hasChildren())
        @include('partials.page.list', ['pages' => $page->pages])
    @endif
@endforeach
