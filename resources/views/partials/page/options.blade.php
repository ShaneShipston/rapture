@foreach ($pages as $item)
    @if (!isset($page) || $item->id !== $page->id)
        <option value="{{ $item->id }}"{{ $selected == $item->id ? 'selected' : '' }}>{{ $item->paddedName() }}</option>
        @if ($item->hasChildren())
            @include('partials.page.options', ['pages' => $item->pages, 'selected' => $selected])
        @endif
    @endif
@endforeach
