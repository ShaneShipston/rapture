<div class="form-field">
    <label>Default Value</label>
    <textarea name="field[{{ $index }}][default]">{{ old('field.' . $index . '.default', isset($block) ? $block->default_value : '') }}</textarea>
</div>