<a href="#" class="remove-field"></a>

<input type="hidden" name="field[{{ $index }}][type]" value="{{ $type }}">
<input type="hidden" name="field[{{ $index }}][order]" value="{{ old('field.' . $index . '.order', isset($block) ? $block->display_order : '-1') }}" class="field-order">
<input type="hidden" name="field[{{ $index }}][id]" value="{{ old('field.' . $index . '.id', isset($block) ? $block->id : '-1') }}">

<div class="form-field">
    <label>Field Label</label>
    <input type="text" name="field[{{ $index }}][label]" class="field-label" value="{{ old('field.' . $index . '.label', isset($block) ? $block->name : '') }}">
</div>

<div class="form-field">
    <label>Instructions</label>
    <textarea name="field[{{ $index }}][description]">{{ old('field.' . $index . '.description', isset($block) ? $block->description : '') }}</textarea>
</div>
