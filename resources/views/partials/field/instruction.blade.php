<div class="field-option{{ $cloned ? ' cloned' : '' }}">

    @include('partials.field.sections.title', ['label' => 'Instructions', 'icon' => 'certificate'])

    <div class="field-body{{ $cloned ? '' : ' slider-hidden'}}">

        @include('partials.field.sections.label', ['type' => 'instruction'])

    </div>
</div>
