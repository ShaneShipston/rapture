<div class="field-option">
    <a href="#" class="field-title"><em class="fa fa-list-ol"></em>Repeater <span class="field-title-display"></span></a>
    <div class="field-body slider-hidden">
        <a href="#" class="remove-field"></a>
        <input type="hidden" name="field[{{ $index }}][type]" value="repeater">
        <input type="hidden" name="field[{{ $index }}][order]" value="-1">
        <div class="form-field">
            <label for="">Field Label</label>
            <input type="text" name="field[{{ $index }}][label]" id="" class="field-label">
        </div>
    </div>
</div>
