<div class="field-option{{ $cloned ? ' cloned' : '' }}">

    @include('partials.field.sections.title', ['label' => 'Text', 'icon' => 'font'])

    <div class="field-body{{ $cloned ? '' : ' slider-hidden'}}">

        @include('partials.field.sections.label', ['type' => 'text'])
        @include('partials.field.sections.default')

        <div class="form-field horizontal">
            <label>Set a limit of</label>
            <input type="number" name="field[{{ $index }}][limit][number]" value="{{ old('field.' . $index . '.limit.number', isset($block) ? $block->getRestriction('number') : '') }}">
            <select name="field[{{ $index }}][limit][type]">
                <option{{ old('field.' . $index . '.limit.type', isset($block) ? $block->getRestriction('type') : '') == 'characters' ? ' selected' : '' }}>characters</option>
                <option{{ old('field.' . $index . '.limit.type', isset($block) ? $block->getRestriction('type') : '') == 'words' ? ' selected' : '' }}>words</option>
            </select>
        </div>

    </div>
</div>
