<div class="field-option{{ $cloned ? ' cloned' : '' }}">

    @include('partials.field.sections.title', ['label' => 'File Upload', 'icon' => 'upload'])

    <div class="field-body{{ $cloned ? '' : ' slider-hidden'}}">

        @include('partials.field.sections.label', ['type' => 'uploader'])

        <div class="form-field horizontal">
            <label>Number of Items</label>
            <input type="number" name="field[{{ $index }}][limit][entries]" value="{{ old('field.' . $index . '.limit.entries', isset($block) ? $block->getRestriction('entries') : '') }}">
            <label>File Types</label>
            <input type="text" name="field[{{ $index }}][limit][extensions]" value="{{ old('field.' . $index . '.limit.types', isset($block) ? $block->getRestriction('extensions') : '') }}" placeholder="Ex. jpg,pdf">
        </div>

    </div>
</div>
