@extends('layouts.content')

@section('content')
    <div class="login">
        <h3>Login</h3>
        <div class="box">
            <form method="POST" action="{{ route('login') }}">
                {{ csrf_field() }}

                <div class="form-field{{ $errors->has('email') ? ' has-error' : '' }}">
                    <label for="email">E-Mail Address</label>
                    <input id="email" type="email" name="email" value="{{ old('email') }}" required autofocus>

                    @if ($errors->has('email'))
                        <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>

                <div class="form-field{{ $errors->has('password') ? ' has-error' : '' }}">
                    <label for="password">Password</label>
                    <input id="password" type="password" name="password" required>

                    @if ($errors->has('password'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
                </div>

                <div class="form-field">
                    <div class="form-checkbox">
                        <input type="checkbox" id="remember" name="remember" {{ old('remember') ? 'checked' : '' }}>
                        <label for="remember">Remember Me</label>
                    </div>
                </div>

                <div class="form-field">
                    <button type="submit" class="btn full">Login</button>
                </div>
            </form>
        </div>

        <p class="align-center">
            <a href="{{ route('password.request') }}">Forgot Your Password?</a>
        </p>
    </div>
@endsection
