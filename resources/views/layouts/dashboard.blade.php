<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{ config('app.name', 'Laravel') }}</title>

        @yield('header-assets')

        <link rel="stylesheet" href="{{ asset('css/app.css') }}">

        <script src="https://cdn.polyfill.io/v2/polyfill.min.js"></script>
    </head>
    <body>
        <div id="wrapper">
            <header class="header">
                <a href="{{ url('/') }}" class="logo">{{ config('app.name', 'Laravel') }}</a>
                <div class="container">
                    <div class="breadcrumbs">
                        {!! \Breadcrumbs::render() !!}
                    </div>
                    <nav class="nav-secondary">
                        <ul class="menu">
                            <li class="menu-item search">
                                <form action="{{ url('/search') }}" id="search-form">
                                    @if (request()->is('projects/*'))
                                    <div class="context">Current Project</div>
                                    @endif
                                    <input type="search" placeholder="Search">
                                </form>
                            </li>
                            <li class="menu-item notifications"><a href="#"><em class="fa fa-bell"></em></a></li>
                            <li class="menu-item profile is-dropdown">
                                <a href="#"><span>{{ auth()->user()->name }}</span></a>
                                <ul class="sub-menu slider-hidden">
                                    <li class="menu-item">
                                        <a href="{{ url('/profile') }}"><em class="fa fa-user-o"></em> My Profile</a>
                                    </li>
                                    <li class="menu-item">
                                        <a href="{{ url('/settings') }}"><em class="fa fa-cog"></em> Account</a>
                                    </li>
                                    <li class="menu-item">
                                        <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><em class="fa fa-power-off"></em> Logout</a>
                                    </li>
                                </ul>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" class="hidden">
                                    {{ csrf_field() }}
                                </form>
                            </li>
                        </ul>
                    </nav>
                </div>
            </header>

            <nav class="nav-primary">
                <ul class="menu">
                    <li class="menu-item{{ request()->is('/') ? ' active' : '' }}">
                        <a href="{{ url('/') }}">
                            <em class="fa fa-dashboard"></em>
                            <span>Dashboard</span>
                        </a>
                    </li>
                    <li class="menu-item{{ request()->is('projects*') ? ' active' : '' }}">
                        <a href="{{ url('projects') }}">
                            <em class="fa fa-th-list"></em>
                            <span>Projects</span>
                        </a>
                    </li>
                    <li class="menu-item{{ request()->is('clients*') ? ' active' : '' }}">
                        <a href="{{ url('clients') }}">
                            <em class="fa fa-address-card-o"></em>
                            <span>Clients</span>
                        </a>
                    </li>
                    <li class="menu-item{{ request()->is('reports*') ? ' active' : '' }}">
                        <a href="{{ url('reports') }}">
                            <em class="fa fa-lightbulb-o"></em>
                            <span>Reports</span>
                        </a>
                    </li>
                    <li class="menu-item{{ request()->is('users*') ? ' active' : '' }}">
                        <a href="{{ url('users') }}">
                            <em class="fa fa-users"></em>
                            <span>Users</span>
                        </a>
                    </li>
                    <li class="menu-item is-dropdown{{ request()->is('system*', 'websites*') ? ' active' : '' }}">
                        <a href="{{ url('system') }}">
                            <em class="fa fa-sitemap"></em>
                            <span>System</span>
                        </a>

                        <ul class="sub-menu{{ request()->is('system*', 'websites*') ? '' : ' slider-hidden' }}">
                            <li class="menu-item">
                                <a href="{{ url('system/types') }}">Types</a>
                            </li>
                            <li class="menu-item">
                                <a href="{{ url('system/stages') }}">Stages</a>
                            </li>
                            <li class="menu-item">
                                <a href="{{ url('websites') }}">Websites</a>
                            </li>
                        </ul>
                    </li>
                </ul>
                <a href="{{ url('changelog') }}" class="app-version">0.1.0-alpha</a>
            </nav>

            <main class="content-main">
                @yield('content')
            </main>
        </div>

        @yield('footer-assets')

        <script src="{{ asset('js/common.js') }}"></script>
        <script src="{{ asset('js/app.js') }}"></script>

        @stack('scripts')
    </body>
</html>
