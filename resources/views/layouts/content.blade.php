<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{ config('app.name', 'Laravel') }}</title>

        @yield('header-assets')

        <link rel="stylesheet" href="{{ asset('css/public.css') }}">

        <script src="https://cdn.polyfill.io/v2/polyfill.min.js"></script>
    </head>
    <body>
        <div id="wrapper">
            <header class="header">
                <div class="container">
                    <a href="#" class="logo">{{ config('app.name', 'Laravel') }}</a>
                </div>
            </header>

            @yield('content')
        </div>

        <script src="{{ asset('tinymce/tinymce.min.js') }}"></script>
        <script src="{{ asset('plupload/plupload.full.min.js') }}"></script>
        <script src="{{ asset('js/common.js') }}"></script>
        <script src="{{ asset('js/public.js') }}"></script>
    </body>
</html>
