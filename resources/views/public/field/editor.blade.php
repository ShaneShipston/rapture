<textarea name="block[{{ $block->id }}]" id="block-{{ $block->id }}" class="editor"{!! $block->hasRestriction('number') ? ' data-limit="' . $block->getRestriction('number') . '" data-type="' . $block->getRestriction('type') . '"' : '' !!}>{{ old('block.' . $block->id, is_null($block->content) ? $block->default_value : $block->content) }}</textarea>
@if ($block->hasRestriction('number'))
    <div class="status-bar">
        <ul class="limitation">
            <li>Maximum <strong>{{ $block->getRestriction('number') }} {{ $block->getRestriction('type') }}</strong></li>
        </ul>
        <div class="feedback"><span class="num">0</span> / {{ $block->getRestriction('number') }} {{ $block->getRestriction('type') }}</div>
    </div>
@endif
