<input type="text" name="block[{{ $block->id }}]" value="{{ old('block.' . $block->id, $block->default_value) }}"{!! $block->hasRestriction('number') ? ' data-limit="' . $block->getRestriction('number') . '" data-type="' . $block->getRestriction('type') . '"' : '' !!}>
@if ($block->hasRestriction('number'))
    <div class="status-bar">
        <ul class="limitation">
            <li>Maximum <strong>{{ $block->getRestriction('number') }} {{ $block->getRestriction('type') }}</strong></li>
        </ul>
        <div class="feedback"><span class="num">0</span> / {{ $block->getRestriction('number') }} {{ $block->getRestriction('type') }}</div>
    </div>
@endif
