<div class="upload-box" data-field-name="block[{{ $block->id }}][]"{!! $block->hasRestriction('entries') ? ' data-limit="' . $block->getRestriction('entries') . '"' : '' !!}{!! $block->hasRestriction('extensions') ? ' data-extensions="' . $block->getRestriction('extensions') . '"' : '' !!}>
    @if (old('block.' . $block->id, $block->content))
        @foreach (old('block.' . $block->id, is_null($block->content) ? null : json_decode($block->content)) as $key => $value)
            <div class="asset complete" id="img-{{ $block->id }}-{{ $key }}">
                <div class="preview" style="background-image: url({{ $value }});"></div>
                <input type="hidden" name="block[{{ $block->id }}][]" value="{{ $value }}">
            </div>
        @endforeach
    @else
        <p class="how-to">Drag and drop a file or click here to get started</p>
    @endif
</div>
@if ($block->hasRestriction('entries') || $block->hasRestriction('extensions'))
    <div class="status-bar">
        <ul class="limitation">
            @if ($block->hasRestriction('entries'))
                <li>Maximum <strong>{{ $block->getRestriction('entries') }}</strong> uploads</li>
            @endif
            @if ($block->hasRestriction('extensions'))
                <li>Allowed file types: <strong>{{ $block->getRestriction('extensions') }}</strong></li>
            @endif
        </ul>
        <div class="feedback"></div>
    </div>
@endif
