@extends('layouts.content')

@section('content')
    <main class="main-content">
        <div class="project-title">
            <div class="container">
                <h2>{{ $project->title }}</h2>
                <a href="{{ url('/content/' . $project->permalink . '/new-page') }}" class="btn"><em class="fa fa-plus"></em> New Page</a>
            </div>
        </div>
        @include('partials.msg.success')

        <div class="project-body">
            <div class="container">

                <table class="table striped">
                    <tr>
                        <th class="status"></th>
                        <th class="label">Title</th>
                        <th class="updated">Last Updated</th>
                        <th class="action"></th>
                    </tr>
                    @include('public.page-list', ['pages' => $pages])
                </table>

                <ul class="legend">
                    <li><span class="status-icon pending"></span>Pending</li>
                    <li><span class="status-icon progress"></span>In Progress</li>
                    <li><span class="status-icon review"></span>Waiting for Review</li>
                    <li><span class="status-icon incomplete"></span>Needs Content</li>
                    <li><span class="status-icon complete"></span>Complete</li>
                </ul>
            </div>
        </div>
    </main>
@endsection
