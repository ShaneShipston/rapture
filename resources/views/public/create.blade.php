@extends('layouts.content')

@section('content')
    <main class="main-content">
        <div class="project-title">
            <div class="container">
                <h2>{{ $project->title }}</h2>
                <a href="{{ url('/content/' . $project->permalink) }}" class="btn-pill"><em class="fa fa-reply"></em> Return to List</a>
            </div>
        </div>

        @include('partials.msg.errors')

        <div class="project-body">
            <div class="container">
                <form action="{{ url('/content/' . $project->permalink) }}" method="post">
                {{ csrf_field() }}

                <div class="row">
                    <div class="content">
                        <div class="box">
                            <div class="form-field">
                                <label for="page-title">Page Title</label>
                                <input type="text" name="title" id="page-title" placeholder="Ex. About Us" value="{{ old('title') }}">
                            </div>
                            <div class="form-field">
                                <label for="page-title">Parent</label>
                                <select name="parent" id="">
                                    <option value="">- None -</option>
                                    @include('partials.page.options', ['pages' => $pages, 'selected' => old('parent')])
                                </select>
                            </div>
                        </div>

                        <div class="box">
                            <div class="form-field">
                                <label>Content</label>
                                <textarea name="block[0]" class="editor" id="block0">{{ old('block.0') }}</textarea>
                            </div>
                            <div class="form-field">
                                <label>Files / Images</label>
                                <div class="upload-box" data-field-name="block[1][]">
                                    @if (old('block.1'))
                                        @foreach (old('block.1') as $key => $value)
                                            <div class="img complete" id="img-1-{{ $key }}">
                                                <div class="preview" style="background-image: url({{ $value }});"></div>
                                                <input type="hidden" name="block[1][]" value="{{ $value }}">
                                            </div>
                                        @endforeach
                                    @else
                                        <p class="how-to">Drag and drop a file or click here to get started</p>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="box">
                            <div class="form-checkbox">
                                <input type="checkbox" name="complete" id="content-complete"@if (old('complete')) checked @endif>
                                <label for="content-complete">Mark this page as complete</label>
                            </div>
                        </div>

                        <div class="section">
                            <button type="submit" class="btn large">Create Page</button>
                        </div>
                    </div>
                </div>
            </form>
            </div>
        </div>
    </main>
@endsection
