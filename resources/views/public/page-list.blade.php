@foreach ($pages as $page)
    <tr class="indent-level-{{ $page->indent }}">
        <td class="status">
            <span class="status-icon {{ $page->status }}" title="{{ $page->statusLabel() }}"></span>
        </td>
        <td class="label">
            <a href="{{ url('/content/' . $project->permalink . '/' . $page->id) }}">{{ $page->name }}</a>
        </td>
        <td class="updated">{{ $page->modified() }}</td>
        <td class="action">
            <a href="{{ url('/content/' . $project->permalink . '/' . $page->id) }}"><em class="fa fa-pencil"></em></a>
            <a href="#" class="delete-btn" data-id="{{ $page->id }}" data-project="{{ $project->permalink }}"><em class="fa fa-trash"></em></a>
        </td>
    </tr>
    @if ($page->pages()->count() > 0)
        @include('public.page-list', ['pages' => $page->pages()->get()])
    @endif
@endforeach
