@extends('layouts.content')

@section('content')
    <main class="main-content">
        <div class="project-title">
            <div class="container">
                <h2>{{ $project->title }}</h2>
                <a href="{{ url('/content/' . $project->permalink) }}" class="btn-pill"><em class="fa fa-reply"></em> Return to List</a>
            </div>
        </div>

        @include('partials.msg.errors')

        <div class="project-body">
            <div class="container">
                <form action="{{ url('/content/' . $project->permalink . '/' . $page->id) }}" method="post">
                {{ csrf_field() }}
                {{ method_field('PUT') }}

                <div class="row">
                    <div class="content">
                        <div class="box">
                            <div class="form-field">
                                <div class="label">
                                    <label for="page-title">Page Title</label>
                                </div>
                                <input type="text" name="title" id="page-title" placeholder="Ex. About Us" value="{{ old('title', $page->name) }}">
                            </div>
                            <div class="form-field">
                                <div class="label">
                                    <label for="page-title">Parent</label>
                                </div>
                                <select name="parent" id="">
                                    <option value="">- None -</option>
                                    @include('partials.page.options', ['pages' => $pages, 'selected' => old('parent', $page->page_id)])
                                </select>
                            </div>
                        </div>

                        <div class="box">
                            @foreach ($blocks as $block)
                                <div class="form-field">
                                    <div class="label">
                                        <label>{{ $block->name }}</label>
                                        @if ($block->description)
                                            <p class="instruction">{!! $block->formattedDescription() !!}</p>
                                        @endif
                                    </div>
                                    @if ($block->type != 'instruction')
                                        @include('public.field.' . $block->type, ['block' => $block])
                                    @endif
                                </div>
                            @endforeach
                        </div>

                        <div class="box">
                            <div class="form-checkbox">
                                <input type="checkbox" name="complete" id="content-complete"@if (old('complete')) checked @endif>
                                <label for="content-complete">Mark this page as complete</label>
                            </div>
                        </div>

                        <div class="section">
                            <button type="submit" class="btn large">Save Changes</button>
                        </div>
                    </div>
                </div>
            </form>
            </div>
        </div>
    </main>
@endsection
