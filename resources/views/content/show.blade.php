@extends('layouts.dashboard')

@push('scripts')
<script>
    const currentPage = {
        project: '{{ $project->permalink }}',
        page: '{{ $page->id }}',
    };
</script>
<script src="{{ asset('tinymce/tinymce.min.js') }}"></script>
<script src="{{ asset('plupload/plupload.full.min.js') }}"></script>
<script src="{{ asset('js/content.js') }}"></script>
@endpush

@section('content')
    <div class="project-header">
        <div class="container">
            <div class="project-details">
                <h2 class="title">Viewing Section</h2>
            </div>
            <div class="project-actions">
                <a href="{{ $project->permalink() }}#content" class="btn-pill"><em class="fa fa-reply"></em> Return to Project</a>
            </div>
        </div>
    </div>

    @include('partials.msg.errors')

    <div class="content-builder">
        <div class="container">
            <form action="{{ $project->permalink('content/' . $page->id . '/save') }}" method="post">
                {{ csrf_field() }}
                {{ method_field('PUT') }}

                <div class="row">
                    <div class="content">
                        <div class="box">
                            <div class="form-field">
                                <label for="page-title">Title</label>
                                <input type="text" name="title" id="page-title" placeholder="Ex. About Us" value="{{ old('title', $page->name) }}">
                            </div>
                        </div>

                        <div class="box">
                            @if ($page->hasBlocks())
                                @foreach ($blocks as $block)
                                    <div class="form-field">
                                        <div class="label">
                                            <label>{{ $block->name }}</label>
                                            @if ($block->description)
                                                <p class="instruction">{!! $block->formattedDescription() !!}</p>
                                            @endif
                                        </div>
                                        @if ($block->type != 'instruction')
                                            @include('public.field.' . $block->type)
                                        @endif
                                    </div>
                                @endforeach
                            @endif
                        </div>

                        <input type="hidden" name="field_count" value="{{ old('field_count', $page->blockTotal()) }}">
                    </div>
                    <div class="sidebar">
                        <button type="submit" class="btn full"><em class="fa fa-floppy-o"></em>Save Changes</button>

                        <div class="widget">
                            <div class="form-field">
                                <label><em class="fa fa-indent"></em>Parent</label>
                                <select name="parent">
                                    <option value="">- None -</option>
                                    @include('partials.page.options', ['selected' => old('parent', $page->page_id)])
                                </select>
                            </div>
                        </div>

                        <div class="widget">
                            <div class="form-field">
                                <label><em class="fa fa-flag-o"></em>Status</label>
                                <select name="status">
                                    <option value="pending"{{ old('status', $page->status) == 'pending' ? ' selected' : '' }}>Pending</option>
                                    <option value="progress"{{ old('status', $page->status) == 'progress' ? ' selected' : '' }}>In Progress</option>
                                    <option value="review"{{ old('status', $page->status) == 'review' ? ' selected' : '' }}>Waiting for Review</option>
                                    <option value="incomplete"{{ old('status', $page->status) == 'incomplete' ? ' selected' : '' }}>Needs Content</option>
                                    <option value="complete"{{ old('status', $page->status) == 'complete' ? ' selected' : '' }}>Complete</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
