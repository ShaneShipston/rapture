@extends('layouts.dashboard')

@push('scripts')
    <script src="{{ asset('js/page.js') }}"></script>
@endpush

@section('content')
    <div class="project-header">
        <div class="container">
            <div class="project-details">
                <h2 class="title">Section Builder</h2>
            </div>
            <div class="project-actions">
                <a href="{{ $project->permalink() }}#content" class="btn-pill"><em class="fa fa-reply"></em> Return to Project</a>
            </div>
        </div>
    </div>

    @include('partials.msg.errors')

    <div class="content-builder">
        <div class="container">
            <form action="{{ $project->permalink('content/' . $page->id) }}" method="post">
                {{ csrf_field() }}
                {{ method_field('PUT') }}

                <div class="row">
                    <div class="content">
                        <div class="box">
                            <div class="form-field">
                                <label for="page-title">Title</label>
                                <input type="text" name="title" id="page-title" placeholder="Ex. About Us" value="{{ old('title', $page->name) }}">
                            </div>
                        </div>

                        <div class="box">
                            <h5 class="box-title">Content</h5>
                            <div class="drop-target" id="new-content">
                                @if (old('field'))
                                    @foreach (old('field') as $index => $field)
                                        @if ($index !== 'index')
                                            @include('partials.field.' . $field['type'], ['cloned' => true, 'index' => $index])
                                        @endif
                                    @endforeach
                                @elseif ($page->blocks()->count() > 0)
                                    @foreach ($page->blocks()->orderBy('display_order')->get() as $index => $field)
                                        @include('partials.field.' . $field->type, ['cloned' => true, 'index' => $index, 'block' => $field])
                                    @endforeach
                                @else
                                    <p class="instructions">Select items from <strong>Field Options</strong> by clicking or dragging.</p>
                                @endif
                            </div>
                        </div>

                        <input type="hidden" name="field_count" value="{{ old('field_count', $page->blocks()->count()) }}">
                    </div>
                    <div class="sidebar">
                        <button type="submit" class="btn full"><em class="fa fa-floppy-o"></em>Save Changes</button>

                        <div class="widget">
                            <div class="form-field">
                                <label><em class="fa fa-indent"></em>Parent</label>
                                <select name="parent" id="">
                                    <option value="">- None -</option>
                                    @include('partials.page.options', ['pages' => $pages, 'selected' => old('parent', $page->page_id)])
                                </select>
                            </div>
                        </div>

                        <div class="widget">
                            <div class="form-field">
                                <label><em class="fa fa-calendar"></em>Deadline</label>
                                <input type="text" name="deadline" id="deadline" class="datepicker" placeholder="(Optional)" value="{{ old('deadline', $page->formattedDeadline('Y-m-d', '')) }}">
                            </div>
                        </div>

                        <div class="widget">
                            <h5 class="widget-title"><em class="fa fa-tasks"></em>Field Options</h5>
                            <div class="field-options" id="content-options">
                                @include('partials.field.instruction', ['cloned' => false, 'index' => 'index'])
                                @include('partials.field.text', ['cloned' => false, 'index' => 'index'])
                                @include('partials.field.editor', ['cloned' => false, 'index' => 'index'])
                                @include('partials.field.uploader', ['cloned' => false, 'index' => 'index'])
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
