@extends('layouts.dashboard')

@section('content')
    <div class="project-header">
        <div class="container">
            <h2 class="title">Clients</h2>
            @if (count($clients) > 0)
            <div class="project-actions">
                <a href="{{ url('clients/create') }}" class="btn"><em class="fa fa-plus"></em> New Client</a>
            </div>
            @endif
        </div>
    </div>

    @include('partials.msg.success')

    <div class="container">
        @if (count($clients) > 0)
        <table class="table striped">
            <tr>
                <th class="mass-action"></th>
                <th>Name</th>
                <th>Address</th>
                <th>City</th>
                <th>Region</th>
                <th class="action"></th>
            </tr>
            @foreach ($clients as $user)
                <tr>
                    <td class="mass-action">
                        <div class="form-checkbox standalone">
                            <input type="checkbox" name="client[]" value="{{ $client->id }}" id="client-action-{{ $client->id }}">
                            <label for="client-action-{{ $client->id }}"></label>
                        </div>
                    </td>
                    <td>{{ $client->name }}</td>
                    <td>{{ $client->address }}</td>
                    <td>{{ $client->city }}</td>
                    <td>{{ $client->region }}</td>
                    <td class="action">
                        <!-- <a href="{{ url('/clients/' . $client->id) }}"><em class="fa fa-eye"></em></a> -->
                        <a href="{{ url('/clients/' . $client->id . '/client') }}"><em class="fa fa-pencil"></em></a>
                        <a href="{{ url('') }}" class="delete-btn" data-id="{{ $client->id }}"><em class="fa fa-trash"></em></a>
                    </td>
                </tr>
            @endforeach
        </table>
        @else
            <div class="row">
                <div class="full centered">
                    <div class="heading">
                        <h3>Create your first client</h3>
                        <p>By creating clients you can organize your projects and allow your clients to monitor their own projects.</p>
                    </div>
                    <p><a href="{{ url('clients/create') }}" class="btn large">Get Started</a>
                </div>
            </div>
        @endif
    </div>
@endsection
