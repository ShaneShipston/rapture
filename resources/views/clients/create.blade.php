@extends('layouts.dashboard')

@push('scripts')
    <script src="{{ asset('js/client.js') }}"></script>
@endpush

@section('content')
    <div class="project-header">
        <div class="container">
            <h2 class="title">New Client</h2>
            <a href="{{ url('clients') }}" class="btn-pill"><em class="fa fa-reply"></em> Return to List</a>
        </div>
    </div>

    <div class="content-builder">
        <form method="post" action="{{ url('users') }}">
            {{ csrf_field() }}

            <div class="tab-wrapper">
                <ul class="tab-headings">
                    <li class="tab active"><a href="#company">Company</a></li>
                    <li class="tab"><a href="#contacts">Contacts</a></li>
                </ul>
            </div>

            <div class="container">
                <div class="row">
                    <div class="w9">

                        @include('partials.msg.errors')

                        <div class="tab-panel active" id="company">
                            <div class="box">
                                <div class="form-field">
                                    <label for="client-name">Name <span class="required">*</span></label>
                                    <input type="text" name="name" id="client-name" value="{{ old('name') }}">
                                </div>
                                <div class="form-field">
                                    <label for="client-address">Address</label>
                                    <input type="email" name="address" id="client-address" value="{{ old('address') }}">
                                </div>
                                <div class="row form-field">
                                    <div class="w6">
                                        <label for="client-city">City</label>
                                        <input type="text" name="city" id="client-city" value="{{ old('city') }}">
                                    </div>
                                    <div class="w3">
                                        <label for="client-province">Province</label>
                                        <input type="text" name="region" id="client-province" value="{{ old('region') }}">
                                    </div>
                                    <div class="w3">
                                        <label for="client-postal">Postal Code</label>
                                        <input type="text" name="postal" id="client-postal" value="{{ old('postal') }}">
                                    </div>
                                </div>
                                <div class="form-field">
                                    <label for="client-phone">Phone Numbers</label>
                                    <textarea name="phone" id="client-phone" placeholder="Place 1 per line">{{ old('phone') }}</textarea>
                                </div>
                            </div>
                        </div>
                        <div class="tab-panel" id="contacts">
                            <div class="box">
                                <div class="form-field">
                                    <label for="contact-name">Name <span class="required">*</span></label>
                                    <input type="text" name="name" id="contact-name" value="{{ old('name') }}">
                                </div>
                                <div class="form-field">
                                    <label for="contact-position">Job Title</label>
                                    <input type="text" name="position" id="contact-position" value="{{ old('position') }}">
                                </div>
                                <div class="form-field">
                                    <label for="contact-email">Email</label>
                                    <input type="email" name="email" id="contact-email" value="{{ old('email') }}">
                                </div>
                                <div class="row form-field">
                                    <div class="w9">
                                        <label for="contact-phone">Phone Number</label>
                                        <input type="text" name="phone" id="contact-phone" value="{{ old('phone') }}">
                                    </div>
                                    <div class="w3">
                                        <label for="contact-extension">Extension</label>
                                        <input type="text" name="extension" id="contact-extension" value="{{ old('extension') }}">
                                    </div>
                                </div>
                                <div class="form-field">
                                    <label for="contact-notes">Notes</label>
                                    <textarea name="notes" id="contact-notes" value="{{ old('notes') }}"></textarea>
                                </div>
                                <div class="form-field">
                                    <div class="form-checkbox">
                                        <input type="checkbox" name="account" id="contact-account">
                                        <label for="contact-account">Create an account for this user</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="w3">
                        <button type="submit" class="btn full">Create Client</button>
                        <button type="button" class="btn muted full add-contact">Add another contact</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection
