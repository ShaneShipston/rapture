@extends('layouts.dashboard')

@push('scripts')
    <script src="{{ asset('tinymce/tinymce.min.js') }}"></script>
    <script src="{{ asset('js/project.js') }}"></script>
@endpush

@section('content')
    <div class="project-header">
        <div class="container">
            <h2 class="title">New Project</h2>
            <a href="{{ url('projects') }}" class="btn-pill"><em class="fa fa-reply"></em> Return to List</a>
        </div>
    </div>

    @include('partials.msg.errors')

    <div class="content-builder">
        <div class="container">
            <form action="{{ url('projects') }}" method="post">
                {{ csrf_field() }}

                <div class="row">
                    <div class="content">
                        <div class="box">
                            <div class="form-field">
                                <label for="project-title">Project Title</label>
                                <input type="text" name="title" id="project-title" value="{{ old('title') }}">
                            </div>
                        </div>

                        <div class="box">
                            <div class="form-field">
                                <label for="project-scope">Scope of Work</label>
                                <textarea name="scope" class="editor" id="project-scope">{{ old('scope') }}</textarea>
                            </div>
                        </div>
                    </div>
                    <div class="sidebar">
                        <button type="submit" class="btn full">Create Project</button>

                        <div class="widget">
                            <div class="form-field">
                                <label for="project-type"><em class="fa fa-tag"></em>Type</label>
                                <select name="type" id="project-type">
                                    <option value="">- Project Type -</option>
                                    <option{{ old('type') == 'Branding' ? ' selected' : '' }}>Branding</option>
                                    <option{{ old('type') == 'Design' ? ' selected' : '' }}>Design</option>
                                    <option{{ old('type') == 'Digital Ads' ? ' selected' : '' }}>Digital Ads</option>
                                    <option{{ old('type') == 'Marketing' ? ' selected' : '' }}>Marketing</option>
                                    <option{{ old('type') == 'Print' ? ' selected' : '' }}>Print</option>
                                    <option{{ old('type') == 'SEO / SEM' ? ' selected' : '' }}>SEO / SEM</option>
                                    <option{{ old('type') == 'Social' ? ' selected' : '' }}>Social</option>
                                    <option{{ old('type') == 'Website' ? ' selected' : '' }}>Website</option>
                                </select>
                            </div>
                        </div>

                        <div class="widget">
                            <div class="form-field">
                                <label for="project-client"><em class="fa fa-address-card-o"></em>Client</label>
                                <input type="text" name="client" id="project-client" value="{{ old('client') }}">
                            </div>
                        </div>

                        <div class="widget">
                            <div class="form-field">
                                <label for="project-stage"><em class="fa fa-list-ol"></em>Stage</label>
                                <select name="stage" id="project-stage">
                                    <option{{ old('stage') == 'Pending' ? ' selected' : '' }}>Pending</option>
                                    <option{{ old('stage') == 'Queued' ? ' selected' : '' }}>Queued</option>
                                    <option{{ old('stage') == 'Doing' ? ' selected' : '' }}>Doing</option>
                                    <option{{ old('stage') == 'Out for Proof' ? ' selected' : '' }}>Out for Proof</option>
                                    <option{{ old('stage') == 'Revisions' ? ' selected' : '' }}>Revisions</option>
                                    <option{{ old('stage') == 'Production Ready' ? ' selected' : '' }}>Production Ready</option>
                                    <option{{ old('stage') == 'Invoice' ? ' selected' : '' }}>Invoice</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
