@extends('layouts.dashboard')

@section('content')
    <div class="project-header">
        <div class="container">
            <h2 class="title">Projects</h2>
            <div class="project-actions">
                <a href="{{ url('projects/create') }}" class="btn"><em class="fa fa-plus"></em> New Project</a>
            </div>
        </div>
    </div>

    @include('partials.msg.success')

    <div class="project-feed">
        <div class="container">
            <!-- Filters (Type, Stage, Assignment) -->

            <div class="faux-table">
                <div class="faux-heading">
                    <div>Project Title</div>
                    <div>Budget</div>
                    <div>Deadline</div>
                    <div>Time Spent</div>
                    <div>Stage</div>
                    <div>Assigned to</div>
                </div>
                @foreach ($projects as $project)
                    <a href="{{ $project->permalink() }}" class="faux-row">
                        <div>
                            <span class="title">{{ $project->title }}</span><br>
                            {{ $project->client }}
                        </div>
                        <div>$6000</div>
                        <div>Sept 1, 2017</div>
                        <div>7 hours</div>
                        <div>Development</div>
                        <div></div>
                    </a>
                @endforeach
            </div>
        </div>
    </div>
@endsection
