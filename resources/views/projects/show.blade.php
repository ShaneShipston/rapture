@extends('layouts.dashboard')

@push('scripts')
    <script src="{{ asset('js/project.js') }}"></script>
@endpush

@section('content')
    <div class="project-header">
        <div class="container">
            <div class="project-details">
                <h2 class="title">{{ $project->title }}</h2>
                <ul class="details">
                    <li>{{ $project->client }}</li>
                    <li>Stage: Development</li>
                    <li>Budget: $6,000</li>
                    @if (!is_null($project->due))
                        <li>Due: {{ $project->formatted_due_date }}</li>
                    @endif
                    <!-- <li class="archived"><em class="fa fa-folder-open-o"></em> Archived</li> -->
                </ul>
            </div>
            <div class="project-actions">
                <a href="#" class="btn-icon fa fa-bookmark-o" data-tooltip="Bookmark"></a>
                <a href="#" class="btn-icon fa fa-pencil" data-tooltip="Edit"></a>
                <a href="#" class="btn-icon fa fa-trash-o" data-tooltip="Delete"></a>
            </div>
        </div>
    </div>

    <div class="project-content">
        <div class="tab-wrapper">
            <ul class="tab-headings">
                <li class="tab"><a href="#overview">Overview</a></li>
                <li class="tab"><a href="#activity">Activity</a></li>
                @if ($project->description)
                <li class="tab"><a href="#scope">Scope of Work</a></li>
                @endif
                <li class="tab"><a href="#issues">Issues</a></li>
                <li class="tab"><a href="#proofs">Proofs</a></li>
                <li class="tab"><a href="#time">Time / Hours</a></li>
                <li class="tab active"><a href="#collection">Content</a></li>
                <li class="tab"><a href="#feedback">Feedback</a></li>
            </ul>
        </div>

        @include('partials.msg.success')

        <div class="tab-body" id="overview"></div>
        <div class="tab-body" id="activity"></div>
        @if ($project->description)
        <div class="tab-body" id="scope">
            {{ $project->description }}
        </div>
        @endif
        <div class="tab-body" id="issues"></div>
        <div class="tab-body" id="proofs"></div>
        <div class="tab-body" id="time"></div>
        <div class="tab-body active" id="collection">
            @if ($pages->count() > 0)
                <div class="row">
                    <div class="content">
                        <table class="table striped">
                            <tr>
                                <th class="mass-action"></th>
                                <th class="status"></th>
                                <th class="label">Title</th>
                                <th class="due">Deadline</th>
                                <th class="assigned">Assigned</th>
                                <th class="updated">Last Updated</th>
                                <th class="action"></th>
                            </tr>
                            @include('partials.page.list', ['pages' => $pages])
                        </table>
                    </div>
                    <div class="sidebar">
                        <a href="{{ $project->permalink('content/create') }}" class="btn full"><em class="fa fa-plus"></em>Add Section</a>
                        <button type="button" class="btn full" data-dialog="#multiple-pages"><em class="fa fa-files-o"></em>Create Multiple Sections</button>

                        <div class="widget">
                            <div class="form-field">
                                <label><em class="fa fa-paper-plane"></em>Send to Client</label>

                                <div class="form-group">
                                    <input type="text" value="{{ url('content/' . $project->permalink) }}" id="send-to-client" readonly>
                                    <button class="btn fa fa-link copy-to-clipboard" data-clipboard-target="#send-to-client"></button>
                                </div>
                            </div>
                        </div>

                        <!-- <div class="timeline">
                            <h3 class="headline">Activity</h3>
                            <ul class="feed">
                                <li class="entry">
                                    <div class="icon"></div>
                                    <div class="body">
                                        <div class="card">
                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Perspiciatis id, vel molestias quisquam inventore, rerum praesentium. Earum delectus est pariatur alias animi, dicta quas dolorem sint cum aliquid, expedita doloremque.</p>
                                            <time>Yesterday</time>
                                        </div>
                                    </div>
                                </li>
                                <li class="entry">
                                    <div class="icon"></div>
                                    <div class="body">
                                        <div class="card">
                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Obcaecati fugit libero quod sed, quam tenetur incidunt nostrum maiores cum alias corporis expedita itaque, a, cumque consequatur officiis. Cupiditate, odit hic.</p>
                                            <time>Yesterday</time>
                                        </div>
                                    </div>
                                </li>
                                <li class="entry">
                                    <div class="icon"></div>
                                    <div class="body">
                                        <div class="card">
                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam architecto et magnam cum dolorum excepturi quos reiciendis culpa qui rem saepe similique impedit, repellat nesciunt officia officiis asperiores repudiandae. Nulla!</p>
                                            <time>Yesterday</time>
                                        </div>
                                    </div>
                                </li>
                                <li class="entry">
                                    <div class="icon"></div>
                                    <div class="body">
                                        <div class="card">
                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Id aut laboriosam repudiandae, eum quam quod dolorem, placeat, minus sapiente nulla, alias officia explicabo distinctio! Iusto, voluptatibus, nesciunt! Autem, molestiae, harum?</p>
                                            <time>Yesterday</time>
                                        </div>
                                    </div>
                                </li>
                                <li class="entry">
                                    <div class="icon"></div>
                                    <div class="body">
                                        <div class="card">
                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quas suscipit placeat ullam error, commodi maxime rerum. Labore iste consequuntur mollitia voluptas voluptatibus asperiores in ut hic, aspernatur alias sed impedit?</p>
                                            <time>Yesterday</time>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                            <a href="#" class="view-all">View All Activity</a>
                        </div> -->
                    </div>
                </div>
            @else
                <div class="row">
                    <div class="full centered">
                        <div class="heading">
                            <h3>Get Started</h3>
                            <p>Provide a rough site structure to make organizing content easier.</p>
                        </div>
                        <p><a href="{{ $project->permalink('content/create') }}" class="btn large"><em class="fa fa-plus"></em>Add Section</a>
                        <button type="button" class="btn large" data-dialog="#multiple-pages"><em class="fa fa-files-o"></em>Create Multiple Sections</button></p>
                    </div>
                </div>
            @endif
        </div>
        <div class="tab-body" id="feedback"></div>
    </div>
@endsection

@section('footer-assets')
    <div id="multiple-pages" class="dialog">
        <div class="dialog-body">
            <form action="{{ url('/projects/' . $project->permalink . '/content/mass') }}" method="post">
                {{ csrf_field() }}
                <div class="dialog-header">
                    <h4 class="dialog-title">Create Multiple Sections</h4>
                    <button type="button" class="dialog-close"><em class="fa fa-times"></em></button>
                </div>
                <div class="dialog-content">
                    <div class="alert-errors hidden"></div>

                    <div class="form-field">
                        <label for="page-list">Structure</label>
                        <textarea name="pages" id="page-list" placeholder="Place each section on a new line"></textarea>
                        <p class="help">Use hyphens to indicate nested sections</p>
                    </div>
                    <div class="form-field">
                        <label for="deadline">Deadline</label>
                        <input type="text" name="deadline" id="deadline" class="datepicker">
                    </div>
                </div>
                <div class="dialog-footer">
                    <button type="submit" class="btn">Create Sections</button>
                </div>
            </form>
        </div>
    </div>
@endsection
