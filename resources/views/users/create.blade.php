@extends('layouts.dashboard')

@section('content')
    <div class="project-header">
        <div class="container">
            <h2 class="title">New User</h2>
            <a href="{{ url('users') }}" class="btn-pill"><em class="fa fa-reply"></em> Return to List</a>
        </div>
    </div>

    @include('partials.msg.errors')

    <div class="content-builder">
        <div class="container">
            <form method="post" action="{{ url('users') }}">
                {{ csrf_field() }}

                <div class="row">
                    <div class="content">
                        <div class="box">
                            <div class="form-field">
                                <label for="userName">Name <span class="required">*</span></label>
                                <input type="text" name="name" id="userName" value="{{ old('name') }}">
                            </div>
                            <div class="form-field">
                                <label for="userEmail">Email <span class="required">*</span></label>
                                <input type="email" name="email" id="userEmail" value="{{ old('email') }}">
                            </div>
                        </div>
                        <div class="box">
                            <div class="form-field">
                                <label for="userPass">Password <span class="required">*</span></label>
                                <input type="password" name="password" id="userPass">
                            </div>
                            <div class="form-field">
                                <label for="userPassConf">Confirm Password <span class="required">*</span></label>
                                <input type="password" name="password_confirmation" id="userPassConf">
                            </div>
                        </div>
                    </div>
                    <div class="sidebar">
                        <button type="submit" class="btn full">Create User</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
