@extends('layouts.dashboard')

@push('scripts')
    <script src="{{ asset('js/user.js') }}"></script>
@endpush

@section('content')
    <div class="project-header">
        <div class="container">
            <h2 class="title">Users</h2>
            <div class="project-actions">
                <a href="{{ url('users/create') }}" class="btn"><em class="fa fa-user-plus"></em> New User</a>
            </div>
        </div>
    </div>

    @include('partials.msg.success')

    <div class="container">

        <!-- Filter by Client -->
        <!-- Filter by Project -->

        <table class="table striped">
            <tr>
                <th class="mass-action"></th>
                <th>Name</th>
                <th>Client</th>
                <th>Email</th>
                <th class="action"></th>
            </tr>
            @foreach ($users as $user)
                <tr>
                    <td class="mass-action">
                        <div class="form-checkbox standalone">
                            <input type="checkbox" name="user[]" value="{{ $user->id }}" id="user-action-{{ $user->id }}">
                            <label for="user-action-{{ $user->id }}"></label>
                        </div>
                    </td>
                    <td>{{ $user->name }}</td>
                    <td>Shout! Media</td>
                    <td>{{ $user->email }}</td>
                    <td class="action">
                        <!-- <a href="{{ url('/users/' . $user->id) }}"><em class="fa fa-eye"></em></a> -->
                        <a href="{{ url('/users/' . $user->id . '/edit') }}"><em class="fa fa-pencil"></em></a>
                        <a href="{{ url('') }}" class="delete-btn" data-id="{{ $user->id }}"><em class="fa fa-trash"></em></a>
                    </td>
                </tr>
            @endforeach
        </table>
    </div>
@endsection
