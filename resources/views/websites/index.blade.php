@extends('layouts.dashboard')

@section('content')
    <div class="project-header">
        <div class="container">
            <h2 class="title">Websites</h2>
            <div class="project-actions">
                <a href="{{ url('websites/create') }}" class="btn"><em class="fa fa-plus"></em> New Website</a>
            </div>
        </div>
    </div>
@endsection
