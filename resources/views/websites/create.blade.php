@extends('layouts.dashboard')

@push('scripts')
    <script src="{{ asset('tinymce/tinymce.min.js') }}"></script>
    <script src="{{ asset('js/project.js') }}"></script>
@endpush

@section('content')
    <div class="project-header">
        <div class="container">
            <h2 class="title">New Website</h2>
            <a href="{{ url('websites') }}" class="btn-pill"><em class="fa fa-reply"></em> Return to List</a>
        </div>
    </div>

    @include('partials.msg.errors')

    <div class="content-builder">
        <div class="container">
            <form action="{{ url('websites') }}" method="post">
                {{ csrf_field() }}

                <div class="row">
                    <div class="content">
                        <div class="box">
                            <div class="form-field">
                                <label for="domain-name">Domain Name</label>
                                <input type="text" name="domain" id="domain-name" value="{{ old('domain') }}" placeholder="Ex. website.com">
                            </div>
                            <div class="form-field">
                                <label for="deployment-server">Server</label>
                                <select name="server" id="deployment-server">
                                    @foreach ($servers as $server)
                                        <option value="{{ $server }}">{{ ucwords($server) }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="sidebar">
                        <button type="submit" class="btn full">Create Website</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
