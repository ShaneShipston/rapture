<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::middleware(['auth'])->group(function () {
    /**
     * Dashboard
     */
    Route::get('/', 'DashboardController@index');

    /**
     * Project specific
     */
    Route::get('projects', 'ProjectsController@index');
    Route::get('projects/create', 'ProjectsController@create');
    Route::get('projects/{project}', 'ProjectsController@show');
    Route::post('projects', 'ProjectsController@store');

    /**
     * Project > Content
     */
    Route::post('projects/{project}/content/mass', 'MassContentController@store');
    Route::post('projects/{project}/content/upload', 'UploadController@store');
    Route::post('projects/{project}/content/download', 'ProjectDownloadController@download');
    Route::get('projects/{project}/content/create', 'ProjectContentController@create');
    Route::post('projects/{project}/content', 'ProjectContentController@store');
    Route::get('projects/{project}/content/{page}', 'ProjectContentController@show');
    Route::get('projects/{project}/content/{page}/edit', 'ProjectContentController@edit');
    Route::put('projects/{project}/content/{page}', 'ProjectContentController@update');
    Route::delete('projects/{project}/content/{page}', 'ProjectContentController@destroy');
    Route::put('projects/{project}/content/{page}/save', 'ProjectContentController@save');

    /**
     * Clients
     */
    Route::resource('clients', 'ClientsController');

    /**
     * Reports
     */
    Route::get('reports', 'ReportsController@index');

    /**
     * Users
     */
    Route::resource('users', 'UsersController');

    /**
     * System
     */
    Route::get('system', 'SystemController@index');
    Route::get('system/types', 'ProjectTypesController@index');
    Route::get('system/states', 'ProjectStagesController@index');

    /**
     * Websites
     */
    Route::get('websites', 'WebsitesController@index');
    Route::get('websites/create', 'WebsitesController@create');
    Route::post('websites', 'WebsitesController@store');

    /**
     * Search
     */
    Route::get('search', 'SearchController@index');

    /**
     * Profile
     */
    Route::get('profile', 'ProfileController@index');

    /**
     * Settings
     */
    Route::get('settings', 'SettingsController@index');

    /**
     * Changelog
     */
    Route::get('changelog', 'ChangelogController@index');
});

/**
 * Content Collection Routes
 */
Route::post('content/upload', 'UploadController@store');
Route::post('content/download', 'ContentDownloadController@download');
Route::get('content/{project}', 'ContentController@show');
Route::get('content/{project}/new-page', 'ContentController@create');
Route::get('content/{project}/{page}', 'ContentController@view');
Route::put('content/{project}/{page}', 'ContentController@update');
Route::post('content/{project}', 'ContentController@store');
Route::delete('content/{project}/{page}', 'ContentController@destroy');

/**
 * Login / Registration
 */
Auth::routes();
