<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $timestamp = Carbon::now();

        $timestamps = [
            'created_at' => $timestamp,
            'updated_at' => $timestamp,
        ];

        // Dashboard
        DB::table('users')->insert(array_merge([
            'name' => 'Shane',
            'email' => 'shane@shout-media.ca',
            'password' => bcrypt('teamshout'),
        ], $timestamps));
    }
}
