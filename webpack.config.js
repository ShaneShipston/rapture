module.exports = {
    entry: {
        app: './resources/assets/js/app',
        public: './resources/assets/js/public',
        page: './resources/assets/js/page',
        project: './resources/assets/js/project',
        content: './resources/assets/js/content',
        client: './resources/assets/js/client',
        user: './resources/assets/js/user',
    },
    output: {
        path: './public/js',
        filename: '[name].js',
        chunkFilename: '[id].chunk.js',
    },
    module: {
        rules: [{
            test: /\.js$/,
            exclude: /node_modules/,
            use: 'babel-loader',
        }],
    },
};
