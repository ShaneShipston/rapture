<?php

namespace Tests\Unit;

use App\Project;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class ProjectTest extends TestCase
{
	use DatabaseMigrations;

	/** @test */
    public function canGetDueDate()
    {
    	// Create project with known date
        $project = factory(Project::class)->create([
        	'due' => Carbon::parse('2017-06-01 12:00pm'),
        ]);

        // Retrieve the formatted date
        $date = $project->formatted_due_date;

        // Verify date
        $this->assertEquals('June 1, 2017', $date);
    }
}
