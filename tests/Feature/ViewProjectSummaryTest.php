<?php

namespace Tests\Feature;

use App\Project;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;

class ViewProjectSummaryTest extends TestCase
{
	use DatabaseMigrations;

	/** @test */
	public function userCanViewProjectPage()
	{
		// Arrange
		// Create Project
		$project = Project::create([
			'title' => 'Rapture',
			'client' => 'Shout! Media',
			'due' => Carbon::parse('May 1, 2017'),
			'description' => 'The greatest project management system in existance.',
		]);

		// Act
		// View the Project
		$response = $this->get('/project/' . $project->id);

		// Assert
		// See Project Details
		$response->assertStatus(200);
		$response->assertSee('Rapture');
		$response->assertSee('Shout! Media');
		$response->assertSee('May 1, 2017');
		$response->assertSee('The greatest project management system in existance.');
	}

	/** @test */
	public function userCanCreateProjectWithoutDueDateOrDescription()
	{
		// Create Project
		$project = Project::create([
			'title' => 'Rapture 2',
			'client' => 'Shout! Media 2',
		]);

		// View the Project
		$response = $this->get('/project/' . $project->id);

		// See Project Details
		$response->assertStatus(200);
		$response->assertSee('Rapture 2');
		$response->assertSee('Shout! Media 2');
	}
}
