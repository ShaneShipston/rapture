const gulp = require('gulp');
const sass = require('gulp-sass');
const prefix = require('gulp-autoprefixer');
const minifycss = require('gulp-clean-css');
const imagemin = require('gulp-imagemin');
const sourcemaps = require('gulp-sourcemaps');
const watch = require('gulp-watch');
const sync = require('browser-sync').create();
const webpackconfig = Object.create(require('./webpack.config.js'));
const webpack = require('webpack');
const gutil = require('gulp-util');

const config = {
    url: 'rapture.app',
    port: 3000,
    src: {
        img: 'resources/assets/img/**/*.{png,jpg,gif}',
        js: 'resources/assets/js/**/*.js',
        css: 'resources/assets/sass/**/*.scss',
    },
    dest: {
        img: 'public/img',
        js: 'public/js',
        css: 'public/css',
    },
    watch: [
        'app/**/*.php',
        'bootstrap/**/*.php',
        'config/**/*.php',
        'database/**/*.php',
        'public/**/*.php',
        'resources/**/*.php',
        'routes/**/*.php',
        'storage/**/*.php',
    ],
};

// --- [DEV TASKS] ---

gulp.task('sync', () => {
    sync.init({
        proxy: config.url,
        port: config.port,
        ui: false,
        online: true,
        logPrefix: 'Rapture',
        open: false,
    });
});

gulp.task('watch', () => {
    // CSS
    watch(config.src.css, () => {
        gulp.start('styles');
    });

    // Images
    watch(config.src.img, { events: ['add'] })
        .pipe(imagemin())
        .pipe(gulp.dest(config.dest.img));

    // PHP
    watch(config.watch, () => {
        sync.reload();
    });

    // JS
    watch(config.src.js, () => {
        gulp.start('javascript');
    });
});

gulp.task('styles', () => {
    gulp.src(config.src.css)
        .pipe(sourcemaps.init())
        .pipe(sass({
            outputStyle: 'compressed',
        }).on('error', sass.logError))
        .pipe(prefix({
            cascade: false,
        }))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest(config.dest.css))
        .pipe(sync.stream({
            match: '**/*.css',
        }));
});

gulp.task('images', () => {
    gulp.src(config.src.img)
        .pipe(imagemin())
        .pipe(gulp.dest(config.dest.img));
});

gulp.task('javascript', (callback) => {
    webpackconfig.plugins = webpackconfig.plugins = [
        new webpack.optimize.UglifyJsPlugin({
            sourceMap: true,
        }),
        new webpack.ContextReplacementPlugin(/moment[\/\\]locale$/, /en-ca/),
        new webpack.optimize.CommonsChunkPlugin({
            filename: 'common.js',
            name: 'common',
        }),
    ];

    webpack(webpackconfig, (err, stats) => {
        if (err) {
            throw new gutil.PluginError('webpack:build', err);
        }

        gutil.log('[webpack:build]', stats.toString({
            colors: true,
        }));

        sync.reload();

        callback();
    });
});

// --- [BUILD TASKS] ---

gulp.task('build', () => {
    gulp.src(config.src.css)
        .pipe(sass({
            outputStyle: 'compressed',
            errLogToConsole: true,
        }))
        .pipe(prefix({
            remove: false,
            cascade: false,
        }))
        .pipe(minifycss())
        .pipe(gulp.dest(config.dest.css));
});

gulp.task('default', ['sync', 'styles', 'javascript', 'images', 'watch']);
