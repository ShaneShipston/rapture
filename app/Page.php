<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Page extends Model
{
    use SoftDeletes;

    protected $guarded = [];

    protected $dates = ['created_at', 'updated_at', 'deadline', 'deleted_at'];

    protected $statuses = [
        'pending' => 'Pending',
        'progress' => 'In Progress',
        'review' => 'Waiting for Review',
        'incomplete' => 'Needs Content',
        'complete' => 'Completed',
    ];

    public function blocks()
    {
        return $this->hasMany('App\Block');
    }

    public function page()
    {
        return $this->belongsTo('App\Page');
    }

    public function pages()
    {
        return $this->hasMany('App\Page');
    }

    /**
     * Return human readable published date
     * @return string Time since
     */
    public function published()
    {
        return $this->created_at->diffForHumans();
    }

    /**
     * Return human readable published date
     * @return string Time since
     */
    public function modified()
    {
        return $this->updated_at->diffForHumans();
    }

    public function deadline()
    {
        return $this->deadline->diffForHumans();
    }

    public function formattedDeadline($format = 'M j', $default = '--')
    {
        return $this->deadline ? $this->deadline->format($format) : $default;
    }

    public function statusLabel()
    {
        return $this->statuses[$this->status];
    }

    public function paddedName()
    {
        return str_repeat('-', $this->indent) . ' ' . $this->name;
    }

    public function blockTotal()
    {
        return $this->blocks()->count();
    }

    public function hasBlocks()
    {
        return $this->blockTotal() > 0;
    }

    public function orderedBlocks()
    {
        return $this->blocks()->orderBy('display_order')->get();
    }

    public function hasChildren()
    {
        return $this->pages()->count() > 0;
    }
}
