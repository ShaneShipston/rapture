<?php

namespace App\Http\Controllers;

use App\Block;
use App\Page;
use App\Project;
use Illuminate\Http\Request;

class MassContentController extends Controller
{
    public function store(Request $request, Project $project)
    {
        $this->validate($request, [
            'pages' => 'required',
        ], [
            'pages.required' => 'Please provide some form of page structure.'
        ]);

        $pages = collect(explode("\n", $request->input('pages')))->map(function ($item, $key) {
            return trim($item);
        });

        $page_ids = collect();
        $indent = 0;

        foreach ($pages as $key => $item) {
            $nesting = strspn($item, '-');
            $name = trim(ltrim($item, '-'));
            $remove_previous_id = true;

            // No nesting on first element or child
            if ($key == 0 && $nesting > 0 || $nesting - $indent >= 1) {
                $remove_previous_id = false;
            }

            // Move back up the tiers
            if ($nesting - $indent < 0) {
                $page_ids = $page_ids->take($page_ids->count() - ($indent - $nesting));
            }

            if ($remove_previous_id) {
                $page_ids->pop();
            }

            $indent = $nesting;

            $page_data = [
                'name' => $name,
                'project_id' => $project->id,
                'page_id' => $page_ids->last(),
                'indent' => $page_ids->count(),
            ];

            if ($request->has('deadline')) {
                $page_data['deadline'] = $request->input('deadline');
            }

            $page = Page::create($page_data);

            Block::create([
                'name' => 'Content',
                'page_id' => $page->id,
                'display_order' => 1,
                'type' => 'editor',
            ]);

            Block::create([
                'name' => 'Files / Images',
                'page_id' => $page->id,
                'display_order' => 2,
                'type' => 'uploader',
            ]);

            $page_ids->push($page->id);
        }

        return response()->json([
            'status' => 'success'
        ]);
    }
}
