<?php

namespace App\Http\Controllers;

use Chumper\Zipper\Zipper;
use Illuminate\Http\Request;

class ContentDownloadController extends Controller
{
    /**
     * Download Assets
     * @todo Validate access
     */
    public function download(Request $request)
    {
        $assets = collect(json_decode($request->input('assets')))->transform(function ($item) {
            return substr($item, 1);
        });

        $filename = 'downloads/' . md5(microtime()) . '.zip';

        $zipfile = new Zipper;
        $zipfile
            ->make($filename)
            ->add($assets->toArray())
            ->close();

        return response()->json([
            'status' => 'success',
            'file' => $filename,
        ]);
    }
}
