<?php

namespace App\Http\Controllers;

use App\Project;
use Chumper\Zipper\Zipper;
use Illuminate\Http\Request;

class ProjectDownloadController extends Controller
{
    public function download(Request $request, Project $project)
    {
        $assets = collect(json_decode($request->input('assets')))->transform(function ($item) {
            return substr($item, 1);
        });

        $filename = 'downloads/' . $project->permalink . '-' . md5(microtime()) . '.zip';

        $zipfile = new Zipper();
        $zipfile
            ->make($filename)
            ->add($assets->toArray())
            ->close();

        return response()->json([
            'status' => 'success',
            'file' => $filename,
        ]);
    }
}
