<?php

namespace App\Http\Controllers;

use App\Project;
use Illuminate\Http\Request;

class ProjectsController extends Controller
{
    public function index()
    {
        \Breadcrumbs::addCrumb('Projects', url('/projects'));

        $projects = Project::all();

        return view('projects.index', [
            'projects' => $projects,
        ]);
    }

    public function show(Project $project)
    {
        $this->breadcrumbs($project->title, url('/project/' . $project->permalink));

        return view('projects.show', [
            'project' => $project,
            'pages' => $project->parentPages(),
        ]);
    }

    public function create()
    {
        $this->breadcrumbs('New Project');

        return view('projects.create');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'type' => 'required',
            'client' => 'required',
        ]);

        Project::create([
            'title' => $request->input('title'),
            'client' => $request->input('client'),
            'description' => $request->input('scope'),
            'permalink' => str_slug($request->input('title')),
        ]);

        return redirect('projects')->with('status', 'Project Created!');
    }

    private function breadcrumbs($name, $url = '')
    {
        \Breadcrumbs::addCrumb('Projects', url('projects'));
        \Breadcrumbs::addCrumb($name, $url);
    }
}
