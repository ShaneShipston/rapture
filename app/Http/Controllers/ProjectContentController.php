<?php

namespace App\Http\Controllers;

use App\Block;
use App\Page;
use App\Project;
use Chumper\Zipper\Zipper;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ProjectContentController extends Controller
{
    /**
     * Create new section
     * @param Project $project
     */
    public function create(Project $project)
    {
        $this->breadcrumb($project, 'New Section');

        return view('content.create', [
            'project' => $project,
            'pages' => $project->parentPages(),
        ]);
    }

    /**
     * Save new section
     * @param Request $request
     * @param Project $project
     */
    public function store(Request $request, Project $project)
    {
        $this->validate($request, [
            'title' => 'required',
            'field' => 'required|array|min:2', // Min 2 refers to the [index] field coming across
        ], [
            'field.min' => 'The page content must have at least 1 field.',
        ]);

        $data = [
            'name' => $request->input('title'),
            'project_id' => $project->id,
            'indent' => 0,
        ];

        if ($request->has('parent')) {
            $parent = Page::find($request->input('parent'));

            $data['page_id'] = $request->input('parent');
            $data['indent'] = $parent->indent + 1;
        }

        if ($request->has('deadline')) {
            $data['deadline'] = $request->input('deadline');
        }

        $page = Page::create($data);

        foreach ($request->input('field') as $key => $field) {
            if ($key !== 'index') {
                $field = collect($field);

                $block = [
                    'name' => $field->get('label') ? $field->get('label') : ucwords($field->get('type')),
                    'page_id' => $page->id,
                    'display_order' => $field->get('order'),
                    'type' => $field->get('type'),
                ];

                if ($field->get('description')) {
                    $block['description'] = $field->get('description');
                }

                if ($field->get('default')) {
                    $block['default_value'] = $field->get('default');
                }

                if ($field->get('limit')) {
                    $block['restrictions'] = $field->get('limit');
                }

                Block::create($block);
            }
        }

        return redirect('projects/' . $project->permalink . '#content')->with('status', 'Page created!');
    }

    /**
     * Edit Section
     * @param Project $project
     * @param Page    $page
     */
    public function edit(Project $project, Page $page)
    {
        $this->breadcrumb($project, 'Edit Section');

        return view('content.edit', [
            'project' => $project,
            'pages' => $project->parentPages(),
            'page' => $page,
        ]);
    }

    /**
     * Save section changes
     * @param Request $request
     * @param Project $project
     * @param Page    $page
     */
    public function update(Request $request, Project $project, Page $page)
    {
        $this->validate($request, [
            'title' => 'required',
            'field' => 'required|array|min:2',
        ], [
            'field.min' => 'The page content must have at least 1 field.',
        ]);

        $blocks = collect($request->input('field'));

        $page->name = $request->input('title');

        if ($request->has('parent')) {
            $parent = Page::find($request->input('parent'));

            $page->page_id = $request->input('parent');
            $page->indent = $parent->indent + 1;
        } else {
            $page->page_id = null;
            $page->indent = 0;
        }

        if ($request->has('deadline')) {
            $page->deadline = $request->input('deadline');
        } else {
            $page->deadline = null;
        }

        $page->save();

        if ($page->hasBlocks()) {
            // Filter out the deleted items
            $to_delete = $page->blocks->filter(function ($value, $key) use ($blocks) {
                return !$blocks->pluck('id')->contains($value->id);
            });

            // Filter out the new items
            $to_create = $blocks->filter(function ($value, $key) {
                return $value['id'] < 0 && $key !== 'index';
            });

            // Filter out items that need to be updated
            $to_update = $blocks->filter(function ($value, $key) {
                return $value['id'] > 0 && $key !== 'index';
            });
        } else {
            $to_create = $request->input('field');
        }

        if (!empty($to_create)) {
            foreach ($to_create as $key => $field) {
                $field = collect($field);

                $block = [
                    'name' => $field->get('label') ? $field->get('label') : ucwords($field->get('type')),
                    'page_id' => $page->id,
                    'display_order' => $field->get('order'),
                    'type' => $field->get('type'),
                ];

                if ($field->get('description')) {
                    $block['description'] = $field->get('description');
                }

                if ($field->get('default')) {
                    $block['default_value'] = $field->get('default');
                }

                if ($field->get('limit')) {
                    $block['restrictions'] = $field->get('limit');
                }

                Block::create($block);
            }
        }

        if (!empty($to_delete)) {
            foreach ($to_delete as $block) {
                $block->delete();
            }
        }

        if (!empty($to_update)) {
            foreach ($to_update as $field) {
                $field = collect($field);

                $block = Block::find($field->get('id'));

                $block->name = $field->get('label') ? $field->get('label') : ucwords($field->get('type'));
                $block->display_order = $field->get('order');

                if ($field->get('description')) {
                    $block->description = $field->get('description');
                } else {
                    $block->description = null;
                }

                if ($field->get('default')) {
                    $block->default_value = $field->get('default');
                } else {
                    $block->default_value = null;
                }

                if ($field->get('limit')) {
                    $block->restrictions = $field->get('limit');
                } else {
                    $block->restrictions = null;
                }

                $block->save();
            }
        }

        return redirect($project->permalink() . '#content')->with('status', 'Page updated!');
    }

    public function show(Project $project, Page $page)
    {
        $this->breadcrumb($project, 'Viewing Page');

        return view('content.show', [
            'project' => $project,
            'pages' => $project->parentPages(),
            'page' => $page,
            'blocks' => $page->orderedBlocks(),
        ]);
    }

    public function save(Request $request, Project $project, Page $page)
    {
        $this->validate($request, [
            'title' => 'required',
        ]);

        $blocks = $page->orderedBlocks();

        if ($request->has('parent')) {
            $parent = Page::find($request->input('parent'));

            $page->page_id = $request->input('parent');
            $page->indent = $parent->indent + 1;
        } else {
            $page->page_id = null;
            $page->indent = 0;
        }

        $page->name = $request->input('title');
        $page->status = $request->input('status');

        $page->save();

        foreach ($blocks as $block) {
            $key = 'block.' . $block->id;

            if ($request->has($key)) {
                if ($block->type === 'uploader') {
                    $block->content = json_encode($request->input($key));
                } else {
                    $block->content = $request->input($key);
                }
            } else {
                $block->content = null;
            }

            $block->save();
        }

        return redirect('projects/' . $project->permalink . '#content')->with('status', 'Changes saved!');
    }

    public function destroy(Request $request, Project $project, Page $page)
    {
        $page->delete();

        return response()->json([
            'status' => 'success'
        ]);
    }

    private function breadcrumb($project, $page)
    {
        \Breadcrumbs::addCrumb('Projects', url('/projects'));
        \Breadcrumbs::addCrumb($project->title, $project->permalink());
        \Breadcrumbs::addCrumb($page);
    }
}
