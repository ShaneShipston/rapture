<?php

namespace App\Http\Controllers;

use App\Client;
use Illuminate\Http\Request;

class ClientsController extends Controller
{
    /**
     * Client Listing
     */
    public function index()
    {
        $clients = Client::all();

        return view('clients.index', [
            'clients' => $clients,
        ]);
    }

    /**
     * Create new client form
     */
    public function create()
    {
        return view('clients.create');
    }

    /**
     * Save client
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|max:255',
        ]);

        $client = Client::create([
            'name' => $request->input('name'),
            'address' => $request->input('address'),
            'city' => $request->input('city'),
            'region' => $request->input('region'),
            'postalcode' => $request->input('postal'),
            'phonenumbers' => $request->input('phone'),
        ]);

        return redirect('clients')->with('status', 'Client was successfully created!');
    }

    /**
     * Edit client
     */
    public function edit(Client $client)
    {
        return view('clients.edit', [
            'client' => $client,
        ]);
    }

    /**
     * Update client
     */
    public function update(Request $request, Client $client)
    {
        $validation = [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255',
        ];

        if ($request->has('password')) {
            $validation['password'] = 'required|min:6|confirmed';
        }

        if ($request->has('email') && $client->email !== $request->input('email')) {
            $validation['email'] = 'required|email|max:255|unique:clients';
        }

        $this->validate($request, $validation);

        $client->name = $request->input('name');
        $client->email = $request->input('email');

        if ($request->has('password')) {
            $client->password = bcrypt($request->input('password'));
        }

        $client->save();

        return redirect('clients')->with('status', 'Client was successfully updated!');
    }

    /**
     * Delete client
     */
    public function destroy(Client $client)
    {
        $client->delete();

        return response()->json([
            'status' => 'success'
        ]);
    }

    /**
     * View client
     */
    public function show(Client $client)
    {
        //
    }
}
