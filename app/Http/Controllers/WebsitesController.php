<?php

namespace App\Http\Controllers;

use Gufy\CpanelWhm\Facades\CpanelWhm;
use Illuminate\Http\Request;

class WebsitesController extends Controller
{
    public function index()
    {
        return view('websites.index');
    }

    public function create()
    {
        return view('websites.create', ['servers' => config('cpanel-whm.servers')]);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'domain' => 'required',
        ]);

        $domain = $request->input('domain');
        $user = str_replace('/[a-z0-9]/', '', strtolower($domain));
        $pass = str_random(15);
        $plan = 'kitchensink';
        $server = $request->input('server');

        $account = CpanelWhm::server($server)->createAccount($domain, $user, $pass, $plan);

        $query_restrictions = CpanelWhm::server($server)->execute_action(3, 'Mysql', 'get_restrictions', $user);

        $db_credentials = json_decode($query_restrictions);
        $prefix = $db_credentials->result->data->prefix;

        $new_db = CpanelWhm::server($server)->execute_action(3, 'Mysql', 'create_database', $user, ['name' => $prefix . '_main']);

        $new_user = CpanelWhm::server($server)->execute_action(3, 'Mysql', 'create_user', $user, ['name' => $prefix . '_wp', 'password' => 'D4Hwi0C;*gpC']);

        $assign_user_to_database = CpanelWhm::server($server)->execute_action(3, 'Mysql', 'set_privileges_on_database', $user, ['user' => $prefix . '_wp', 'database' => $prefix . '_main', 'privileges' => 'ALL PRIVILEGES']);
    }
}
