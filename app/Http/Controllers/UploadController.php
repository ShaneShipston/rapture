<?php

namespace App\Http\Controllers;

use App\Project;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class UploadController extends Controller
{
    public function store(Request $request, Project $project)
    {
        $path = $request->file('plupload')->store('public');

        return Storage::url($path);
    }
}
