<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class UsersController extends Controller
{
    /**
     * User Listing
     */
    public function index()
    {
        $users = User::all();

        return view('users.index', [
            'users' => $users,
        ]);
    }

    /**
     * Create new user form
     */
    public function create()
    {
        return view('users.create');
    }

    /**
     * Save user
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
        ]);

        $user = User::create([
            'name' => $request->input('name'),
            'email' => $request->input('email'),
            'password' => bcrypt($request->input('password')),
        ]);

        return redirect('users')->with('status', 'User was successfully created!');
    }

    /**
     * Edit User
     */
    public function edit(User $user)
    {
        return view('users.edit', [
            'user' => $user,
        ]);
    }

    /**
     * Update User
     */
    public function update(Request $request, User $user)
    {
        $validation = [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255',
        ];

        if ($request->has('password')) {
            $validation['password'] = 'required|min:6|confirmed';
        }

        if ($request->has('email') && $user->email !== $request->input('email')) {
            $validation['email'] = 'required|email|max:255|unique:users';
        }

        $this->validate($request, $validation);

        $user->name = $request->input('name');
        $user->email = $request->input('email');

        if ($request->has('password')) {
            $user->password = bcrypt($request->input('password'));
        }

        $user->save();

        return redirect('users')->with('status', 'User was successfully updated!');
    }

    /**
     * Delete User
     */
    public function destroy(User $user)
    {
        $user->delete();

        return response()->json([
            'status' => 'success'
        ]);
    }

    /**
     * View User
     */
    public function show(User $user)
    {
        //
    }
}
