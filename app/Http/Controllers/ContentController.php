<?php

namespace App\Http\Controllers;

use App\Block;
use App\Page;
use App\Project;
use Illuminate\Http\Request;

class ContentController extends Controller
{
    /**
     * View Project
     */
    public function show(Project $project)
    {
        $pages = $project->pages()->where('page_id', null)->get();

        return view('public.show', [
            'project' => $project,
            'pages' => $pages,
        ]);
    }

    /**
     * Create new Page
     */
    public function create(Project $project)
    {
        $pages = $project->pages()->where('page_id', null)->get();

        return view('public.create', [
            'project' => $project,
            'pages' => $pages,
        ]);
    }

    /**
     * Edit Page
     */
    public function view(Project $project, Page $page)
    {
        $pages = $project->pages()->where('page_id', null)->get();
        $blocks = $page->blocks()->orderBy('display_order')->get();

        return view('public.view', [
            'project' => $project,
            'page' => $page,
            'pages' => $pages,
            'blocks' => $blocks,
        ]);
    }

    /**
     * Save page changes
     */
    public function update(Request $request, Project $project, Page $page)
    {
        $this->validate($request, [
            'title' => 'required',
        ]);

        $blocks = $page->blocks()->orderBy('display_order')->get();

        if ($request->has('parent')) {
            $parent = Page::find($request->input('parent'));

            $page->page_id = $request->input('parent');
            $page->indent = $parent->indent + 1;
        } else {
            $page->page_id = null;
            $page->indent = 0;
        }

        $page->name = $request->input('title');

        if ($request->has('complete') && $page->status != 'complete') {
            $page->status = 'review';
        } else {
            $page->status = 'progress';
        }

        $page->save();

        foreach ($blocks as $block) {
            $key = 'block.' . $block->id;

            if ($request->has($key)) {
                if ($block->type === 'uploader') {
                    $block->content = json_encode($request->input($key));
                } else {
                    $block->content = $request->input($key);
                }
            } else {
                $block->content = null;
            }

            $block->save();
        }

        return redirect('content/' . $project->permalink)->with('status', 'Changes saved!');
    }

    /**
     * Create Page
     */
    public function store(Request $request, Project $project)
    {
        $this->validate($request, [
            'title' => 'required',
        ]);

        $page_data = [
            'name' => $request->input('title'),
            'project_id' => $project->id,
        ];

        if ($request->has('parent')) {
            $parent = Page::find($request->input('parent'));

            $page_data['page_id'] = $request->input('parent');
            $page_data['indent'] = $parent->indent + 1;
        }

        if ($request->has('complete') && $page->status != 'complete') {
            $page_data['status'] = 'review';
        } else {
            $page_data['status'] = 'progress';
        }

        $page = Page::create($page_data);

        Block::create([
            'name' => 'Content',
            'page_id' => $page->id,
            'display_order' => 1,
            'type' => 'editor',
            'content' => $request->input('block.0'),
        ]);

        Block::create([
            'name' => 'Files / Images',
            'page_id' => $page->id,
            'display_order' => 2,
            'type' => 'uploader',
            'content' => $request->has('block.1') ? json_encode($request->input('block.1')) : null,
        ]);

        return redirect('content/' . $project->permalink)->with('status', 'Page created!');
    }

    /**
     * Delete Page
     */
    public function destroy(Request $request, Project $project, Page $page)
    {
        $page->delete();

        return response()->json([
            'status' => 'success'
        ]);
    }
}
