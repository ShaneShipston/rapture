<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    protected $guarded = [];

    protected $dates = ['created_at', 'updated_at', 'due'];

    /**
     * Get the route key for the model.
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'permalink';
    }

    /**
     * Page relationship
     * @return object
     */
    public function pages()
    {
        return $this->hasMany('App\Page');
    }

    /**
     * Formatted due date
     * @return string
     */
    public function getFormattedDueDateAttribute()
    {
        return $this->due->format("F j, Y");
    }

    /**
     * Generate permalink
     * @return void
     */
    public function makePermalink()
    {
        $this->permalink = str_slug($this->name);
    }

    /**
     * Project permalink helper
     * @param  string $section URI Fragment
     * @return string
     */
    public function permalink($section = '')
    {
        if (substr($section, 0, 1) !== '/') {
            $section = '/' . $section;
        }

        return url('projects/' . $this->permalink . $section);
    }

    public function parentPages()
    {
        return $this->pages()->where('page_id', null)->get();
    }
}
