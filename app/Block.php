<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Block extends Model
{
    protected $guarded = [];

    protected $touches = ['page'];

    protected $casts = [
        'restrictions' => 'json',
    ];

    public function page()
    {
        return $this->belongsTo('App\Page');
    }

    public function formattedDescription()
    {
        return nl2br($this->description);
    }

    public function hasRestriction($key)
    {
        return !is_null($this->restrictions) && !empty($this->restrictions[$key]);
    }

    public function getRestriction($key)
    {
        return $this->hasRestriction($key) ? $this->restrictions[$key] : '';
    }

    /**
     * Lowercase file extensions
     * Trim any periods
     * Remove spacing between file extensions
     */
    public function setRestrictionsAttribute($value)
    {
        if (array_key_exists('extensions', $value)) {
            $value['extensions'] = str_replace([' ', '.'], '', strtolower($value['extensions']));
        }

        $this->attributes['restrictions'] = json_encode($value);
    }
}
